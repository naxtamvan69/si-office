$(document).ready(function () {
    $("select").change (function () {
        var selectCriteria = $(this).children("option:selected").val();
        if (selectCriteria === "perihal") {
            $("input").clone().attr('type','text').insertAfter("input").prev().remove();
        } else if (selectCriteria === "tanggal") {
            $("input").clone().attr('type','date').insertAfter("input").prev().remove();
        }
    })
})