$(document).ready(function () {
    var dataFaq;
    var listFaq = [];
    $.ajax({
        url: '/api/faq',
        success: function (data) {
            if (data.status === 200) {
                dataFaq = data.result;
            }
        }
    })

    $('.naik').click(function () {
        var faq = $(this),
            acr1 = faq.parents('div.accordion'),
            acr2 = acr1.prev();
        if (acr2.attr("id") !== undefined) {
            var dictAcr1 = $.fn.getInfoAccordionFaq(acr1),
                dictAcr2 = $.fn.getInfoAccordionFaq(acr2);

            dictAcr1["urutanUpdate"] = dictAcr2["urutanSebelum"];
            dictAcr2["urutanUpdate"] = dictAcr1["urutanSebelum"];
            $.fn.movePositionFaq(dictAcr1);
            $.fn.movePositionFaq(dictAcr2);
            acr1.attr("id", "id-" + dictAcr1["id"] + " " + "urutan-" + dictAcr1["urutanUpdate"])
            acr2.attr("id", "id-" + dictAcr2["id"] + " " + "urutan-" + dictAcr2["urutanUpdate"])
            acr1.after(acr2);

            if (dictAcr1.urutanUpdate != 1 && dictAcr1.urutanUpdate != dataFaq.size()) {
                arc1.children('a.naik').addClass('active');
                arc1.children('a.turun').addClass('active');
            } else if (dictAcr1.urutanUpdate == 1 && dictAcr1.urutanUpdate != dataFaq.size()) {
                arc1.children('a.naik').removeClass('active');
                arc1.children('a.turun').addClass('active');
            } if (dictAcr1.urutanUpdate != 1 && dictAcr1.urutanUpdate == dataFaq.size()) {
                arc1.children('a.naik').removeClass('active');
                arc1.children('a.turun').addClass('active');
            }

        }
    })

    $('.turun').click(function () {
        var faq = $(this),
            acr1 = faq.parents('div.accordion'),
            acr2 = acr1.next();
        if (acr2.attr("id") !== undefined) {
            var dictAcr1 = $.fn.getInfoAccordionFaq(acr1),
                dictAcr2 = $.fn.getInfoAccordionFaq(acr2);
            dictAcr1["urutanUpdate"] = dictAcr2["urutanSebelum"];
            dictAcr2["urutanUpdate"] = dictAcr1["urutanSebelum"];
            $.fn.movePositionFaq(dictAcr1);
            $.fn.movePositionFaq(dictAcr2);
            acr1.attr("id", "id-" + dictAcr1["id"] + " " + "urutan-" + dictAcr1["urutanUpdate"])
            acr2.attr("id", "id-" + dictAcr2["id"] + " " + "urutan-" + dictAcr2["urutanUpdate"])
            acr1.before(acr2);
        }
    })

    $('.submit').click(function () {
        $.ajax({
            type: "POST",
            url: '/api/faq',
            contentType: "application/json",
            data: JSON.stringify({ data: listFaq}),
            success: function (data) {
                if (data.status === 200) {
                    dataFaq = data.result;
                    alert("Ubah urutan faq berhasil");
                    window.location.href = "/faq";
                }
            }
        })
    })

    $.fn.movePositionFaq = function (dictAcr) {
        const idxListFaq = listFaq.findIndex((it) => it.id === dictAcr["id"]);
        if (idxListFaq < 0) {
            listFaq.push(dictAcr);
        } else {
            listFaq[idxListFaq]["urutanSebelum"] = dictAcr["urutanSebelum"];
            listFaq[idxListFaq]["urutanUpdate"] = dictAcr["urutanUpdate"];
        }
    }

    $.fn.getInfoAccordionFaq = function (accordion) {
        var id = accordion.attr("id"),
            listId = id.split(" "),
            idFaq = listId[0].split("-"),
            noUrutan = listId[1].split("-"),
            dictFaq = {};
        dictFaq[idFaq[0]] = parseInt(idFaq[1]);
        dictFaq[noUrutan[0]+"Sebelum"] = parseInt(noUrutan[1]);
        return dictFaq;
    }

    $('.slider').click(function () {
        var self = $(this),
            acr = self.parents('div.accordion'),
            ans = acr.children('div.answer');
        ans.toggleClass('active');
    })
})