$(document).ready(function () {
    $("select").change (function () {
        var selectCriteria = $(this).children("option:selected").val();
        var input = $('.inputCriteria');
        if (selectCriteria === "perihal") {
            input.children("input").remove();
            input.append('<input class="form-control form-control-lg" type="text" name="input" placeholder="Input Nama Perihal" required>');
        } else if (selectCriteria === "tanggal-pengajuan" || selectCriteria === "tanggal-persetujuan-manajer") {
            input.children("input").remove();
            input.append('<input class="form-control form-control-lg" type="date" name="input" placeholder="Input Tanggal Pengajuan" required>');
        } else if (selectCriteria === "ditolak-direktur" || selectCriteria === "disetujui-direktur") {
            input.children("input").remove();
        }
    })
})