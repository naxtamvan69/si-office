$(document).ready(function () {
    var file;
    var nomor = $('#nomor').val();
    var perihal = $('#perihal').val();
    var pengirim = $('#pengirim').val();
    var penerima = $('#penerima').val();

    $(document).on('change', '#file',  function () {
        file = $(this);
        var atr = $(this).siblings('div#file-field');
        $.fn.checkFileField(file, atr);
    })

    $("#nomor").keyup(function () {
        nomor = $(this).val();
        var atr = $(this).siblings('div#nomor-field');
        $.fn.checkTextField(nomor, atr);
    })

    $("#perihal").keyup(function () {
        perihal = $(this).val();
        var atr = $(this).siblings('div#perihal-field');
        $.fn.checkTextField(perihal, atr);
    })

    $("#pengirim").keyup(function () {
        pengirim = $(this).val();
        var atr = $(this).siblings('div#pengirim-field');
        $.fn.checkTextField(pengirim, atr);
    })

    $("#penerima").keyup(function () {
        penerima = $(this).val();
        var atr = $(this).siblings('div#penerima-field');
        $.fn.checkTextField(penerima, atr);
    })

    $('#submit').on('submit', function (e) {
        const checkNomor = $.fn.checkTextField(nomor, $('div#nomor-field'));
        const checkPerihal = $.fn.checkTextField(perihal, $('div#perihal-field'));
        const checkPengirim = $.fn.checkTextField(pengirim, $('div#pengirim-field'))
        const checkPenerima = $.fn.checkTextField(penerima, $('div#penerima-field'))
        const checkFile =  $.fn.checkFileField(file, $('div#file-field'));
        if (checkNomor === true || checkPerihal === true ||
            checkPengirim === true || checkPenerima === true ||
            checkFile === true) {
            alert("Terdapat field yang tidak sesuai dengan ketentuan")
            e.preventDefault()
        }
    })

    $.fn.checkTextField = function (val, atr) {
        if (val === '' || val === undefined) {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else {
            atr.removeClass('active')
            return false;
        }
    }

    $.fn.checkFileField = function (val, atr) {
        if (val === undefined) {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else if (val.val() === '') {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else {
            atr.removeClass('active')
            return false;
        }
    }
})