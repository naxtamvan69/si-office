$(document).ready(function () {
    var file;
    var nomor = $('#nomor').val();
    var perihal = $('#perihal').val();
    var sizeFile;

    $(document).on('change', '#file',  function () {
        file = $(this);
        console.log(file.val())
        var atr = $(this).siblings('div#file-field');
        $.fn.checkFileField(file, atr);
        if (file !== undefined) {
            if (file.val() !== '') {
                sizeFile = (file[0].files[0].size / 1024 / 1024).toFixed(2);
                $.fn.checkSizeField(sizeFile, atr);
            }
        }
    })

    $("#nomor").keyup(function () {
        nomor = $(this).val();
        var atr = $(this).siblings('div#nomor-field');
        $.fn.checkTextField(nomor, atr);
    })

    $("#perihal").keyup(function () {
        perihal = $(this).val();
        var atr = $(this).siblings('div#perihal-field');
        $.fn.checkTextField(perihal, atr);
    })

    $('#submit').on('submit', function (e) {
        const checkNomor = $.fn.checkTextField(nomor, $('div#nomor-field'));
        const checkPerihal = $.fn.checkTextField(perihal, $('div#perihal-field'));
        if (checkNomor === true || checkPerihal === true || checkFile === true) {
            alert("Terdapat field yang tidak sesuai dengan ketentuan")
            e.preventDefault()
        }
    })

    $.fn.checkTextField = function (val, atr) {
        if (val === '' || val === undefined) {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else {
            atr.removeClass('active')
            return false;
        }
    }

    $.fn.checkFileField = function (val, atr) {
        if (val === undefined) {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else if (val.val() === '') {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else {
            atr.removeClass('active')
            return false;
        }
    }
})