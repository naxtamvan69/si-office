$(document).ready(function () {
    var file;
    var nomor = $('#nomor').val();
    var perihal = $('#perihal').val();
    var pengirim = $('#pengirim').val();
    var penerima = $('#penerima').val();
    var sizeFile;
    var selectFile = $('#select-file').children("option:selected").val();
    var filename = $('p').html()

    $('#select-file').change(function () {
        selectFile = $(this).children("option:selected").val();
        if (selectFile === "new-file") {
            var atr = $(this).siblings('div#file-field');
            atr.removeClass('active');
            $(this).siblings('p').remove()
            $(this).after('<input type="file" style="display: inline" class="form-control-file" name="fileSuratMasuk" accept="application/pdf" id="file">');
        } else if (selectFile === "exist-file") {
            var atr = $(this).siblings('div#file-field');
            atr.removeClass('active');
            $(this).siblings('#file').remove();
            $(this).after('<p class="col-auto" style="font-weight: bold; display: inline">'+ filename +'</p>');
        }
    })

    $('.delete').click(function () {
        var self = $(this);
        val = self.attr("value")
    })

    $('.cDeleteSuratMasuk').click(function () {
        $.ajax({
            type: "POST",
            url: '/api/surat-masuk/delete/' + val,
            data: {id:val},
            success: function (data) {
                if (data.status === 200) {
                    alert("Surat Masuk berhasil dihapus")
                    window.location.href = '/surat-masuk'
                }
            }
        })
    })

    $(document).on('change', '#file',  function () {
        file = $(this);
        var atr = $(this).siblings('div#file-field');
        $.fn.checkFileField(file, atr);
        if (file !== undefined) {
            if (file.val() !== '') {
                sizeFile = (file[0].files[0].size / 1024 / 1024).toFixed(2);
                $.fn.checkSizeField(sizeFile, atr);
            }
        }
    })

    $("#nomor").keyup(function () {
        nomor = $(this).val();
        var atr = $(this).siblings('div#nomor-field');
        $.fn.checkTextField(nomor, atr);
    })

    $("#perihal").keyup(function () {
        perihal = $(this).val();
        var atr = $(this).siblings('div#perihal-field');
        $.fn.checkTextField(perihal, atr);
    })

    $("#pengirim").keyup(function () {
        pengirim = $(this).val();
        var atr = $(this).siblings('div#pengirim-field');
        $.fn.checkTextField(pengirim, atr);
    })

    $("#penerima").keyup(function () {
        penerima = $(this).val();
        var atr = $(this).siblings('div#penerima-field');
        $.fn.checkTextField(penerima, atr);
    })

    $('#submit-edit').on('submit', function (e) {
        const checkNomor = $.fn.checkTextField(nomor, $('div#nomor-field'));
        const checkPerihal = $.fn.checkTextField(perihal, $('div#perihal-field'));
        const checkPengirim = $.fn.checkTextField(pengirim, $('div#pengirim-field'))
        const checkPenerima = $.fn.checkTextField(penerima, $('div#penerima-field'))
        if (selectFile === "new-file") {
            const checkFile =  $.fn.checkFileField(file, $('div#file-field'));
            var checkSizeFile = true;
            if (file !== undefined) {
                if (file.val() !== '')
                    checkSizeFile = $.fn.checkSizeField(sizeFile, $('div#file-field'));
            }
            if (checkNomor === true || checkPerihal === true ||
                checkPengirim === true || checkPenerima === true ||
                checkFile === true || checkSizeFile === true) {
                alert("Terdapat field yang tidak sesuai dengan ketentuan")
                e.preventDefault()
            }
        } else {
            if (checkNomor === true || checkPerihal === true ||
                checkPengirim === true || checkPenerima === true) {
                alert("Terdapat field yang tidak sesuai dengan ketentuan")
                e.preventDefault()
            }
        }
    })

    $('#submit-unggah').on('submit', function (e) {
        const checkNomor = $.fn.checkTextField(nomor, $('div#nomor-field'));
        const checkPerihal = $.fn.checkTextField(perihal, $('div#perihal-field'));
        const checkPengirim = $.fn.checkTextField(pengirim, $('div#pengirim-field'))
        const checkPenerima = $.fn.checkTextField(penerima, $('div#penerima-field'))
        const checkFile =  $.fn.checkFileField(file, $('div#file-field'));
        var checkSizeFile = true;
        if (file !== undefined) {
            if (file.val() !== '')
                checkSizeFile = $.fn.checkSizeField(sizeFile, $('div#file-field'));
        }
        if (checkNomor === true || checkPerihal === true ||
            checkPengirim === true || checkPenerima === true ||
            checkFile === true || checkSizeFile === true) {
            alert("Terdapat field yang tidak sesuai dengan ketentuan")
            e.preventDefault()
        }
    })

    $.fn.checkTextField = function (val, atr) {
        if (val === '' || val === undefined) {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else {
            atr.removeClass('active')
            return false;
        }
    }

    $.fn.checkFileField = function (val, atr) {
        if (val === undefined) {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else if (val.val() === '') {
            var child = atr.children().children().children().children('h1');
            child.text('terdapat field yang tidak diisi')
            atr.addClass('active');
            return true;
        } else {
            atr.removeClass('active')
            return false;
        }
    }

    $.fn.checkSizeField = function (val, atr) {
        if (val >= 5) {
            var child = atr.children().children().children().children('h1');
            child.text('Ukuran file yang diunggah lebih dari 5 MB');
            atr.addClass('active');
            return true;
        } else {
            atr.removeClass('active')
            return false;
        }
    }

})