package com.sioffice.service;

import com.sioffice.model.KeluargaModel;

import java.util.List;

public interface KeluargaService {
    List<KeluargaModel> listKeluarga();
    void addKeluarga(KeluargaModel keluarga);
    KeluargaModel getKeluargaById(Integer id);
    void updateKeluarga(KeluargaModel Keluarga);
    void deleteKeluarga(KeluargaModel Keluarga);
}
