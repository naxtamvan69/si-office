package com.sioffice.service;


import com.sioffice.model.FileRegulasiEntity;
import com.sioffice.model.RegulasiModel;
import com.sioffice.repository.FileRegulasiEntityDb;
import com.sioffice.repository.RegulasiDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.mock.web.MockMultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RegulasiServiceImpl implements RegulasiService{
    @Autowired
    private RegulasiDb regulasiDb;

    @Autowired
    private FileRegulasiEntityDb fileRegulasiEntityDb;

    @Override
    public List<RegulasiModel> getlistRegulasi() {
        return regulasiDb.findAll();
    }

    @Override
    public void saveFileRegulasi(MultipartFile file, FileRegulasiEntity fileRegulasi) throws IOException {
        fileRegulasi.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        fileRegulasi.setContentType(file.getContentType());
        fileRegulasi.setData(file.getBytes());
        fileRegulasi.setSize(file.getSize());

        fileRegulasiEntityDb.save(fileRegulasi);
    }

    @Override
    public FileRegulasiEntity getFileRegulasi(String id) {
        Optional<FileRegulasiEntity> fileRegulasi = fileRegulasiEntityDb.findById(id);
        return fileRegulasi.orElse(null);
    }

    @Override
    public List<FileRegulasiEntity> getListFileRegulasi() {
        return fileRegulasiEntityDb.findAll();
    }

    @Override
    public void addRegulasi(RegulasiModel regulasi) {
        regulasiDb.save(regulasi);
    }

    @Override
    public List<RegulasiModel> getListRegulasiByPerihal(String inputPerihal) {
        List<RegulasiModel> getListRegulasi= regulasiDb.findByFilterPerihal(inputPerihal.toLowerCase());
        return getListRegulasi;
    }

    @Override
    public List<RegulasiModel> getListRegulasiByDate(LocalDate inputDate) {
        List<RegulasiModel> getListRegulasi= regulasiDb.findByFilterDate(inputDate);
        return getListRegulasi;
    }

    @Override
    public void deleteRegulsi(RegulasiModel regulasi) {
        regulasiDb.delete(regulasi);
    }

    @Override
    public RegulasiModel getRegulasi(Long id) {
        Optional<RegulasiModel> regulasi = regulasiDb.findByIdRegulasi(id);
        return regulasi.orElse(null);
    }

    @Override
    public MultipartFile convertFileRegulasiToMultiPart(FileRegulasiEntity fileRegulasi) {
        return new MockMultipartFile(fileRegulasi.getName(), fileRegulasi.getName(),
                fileRegulasi.getContentType(), fileRegulasi.getData());
    }

    @Override
    public void editRegulasi(RegulasiModel regulasi) {
        regulasiDb.save(regulasi);
    }

    @Override
    public void editFileRegulasi(MultipartFile file, FileRegulasiEntity fileRegulasi) throws IOException {
        fileRegulasi.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        fileRegulasi.setContentType(file.getContentType());
        fileRegulasi.setData(file.getBytes());
        fileRegulasi.setSize(file.getSize());

        fileRegulasiEntityDb.save(fileRegulasi);
    }

    @Override
    public List<RegulasiModel> getListRegulasiByPerihalAndPageAndSize(String inputPerihal, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("idRegulasi").descending());
        Page<RegulasiModel> pages = regulasiDb.findByFilterPerihalAndPageAndSize(inputPerihal.toLowerCase(), pageable);
        return pages.getContent();
    }

    @Override
    public List<RegulasiModel> getListRegulasiByDateAndPageAndSize(LocalDate inputTanggal, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("idRegulasi").descending());
        Page<RegulasiModel> pages = regulasiDb.findByFilterDateAndPageAndSize(inputTanggal, pageable);
        return pages.getContent();
    }

    @Override
    public List<RegulasiModel> getListRegulasiByPageAndSize(int page, int size) {
        Page<RegulasiModel> pages = regulasiDb.findAll(PageRequest.of(page, size, Sort.by("idRegulasi").descending()));
        return pages.getContent();
    }
}
