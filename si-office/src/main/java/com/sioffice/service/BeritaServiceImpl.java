package com.sioffice.service;

import com.sioffice.model.BeritaModel;
import com.sioffice.model.FaqModel;
import com.sioffice.repository.BeritaDb;
import com.sioffice.repository.PenggunaDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.List;

@Service
@Transactional
public class BeritaServiceImpl implements BeritaService {
    @Autowired
    private BeritaDb beritaDb;

    @Autowired
    private PenggunaDb penggunaDb;

    @Override
    public List<BeritaModel> getListBerita(){
        return beritaDb.findAll();
    }

    @Override
    public void addBerita(BeritaModel berita){
        beritaDb.save(berita);
    }

    @Override
    public BeritaModel getBeritaByNoBerita(long noBerita){
        Optional<BeritaModel> berita = beritaDb.findByNoBerita(noBerita);
        if(berita.isPresent()){
            return berita.get();
        } else{
            return null;
        }
    }

    @Override
    public void save(BeritaModel berita){ beritaDb.save(berita);
    }

    @Override
    public void delete(BeritaModel berita) {beritaDb.delete(berita);}
}
