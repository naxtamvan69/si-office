package com.sioffice.service;

import com.sioffice.model.VendorModel;
import com.sioffice.model.ProdukVendorModel;
import com.sioffice.repository.ProdukVendorDb;
import com.sioffice.repository.VendorDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProdukVendorServiceImpl implements ProdukVendorService {
    @Autowired
    private VendorDb vendorDb;
    @Autowired
    private ProdukVendorDb produkVendorDb;

    @Override
    public List<ProdukVendorModel> listProdukVendor(){
        return produkVendorDb.findAll();
    }

    @Override
    public void addProdukVendor(ProdukVendorModel produkVendor){
        produkVendorDb.save(produkVendor);
    }

    @Override
    public ProdukVendorModel getProdukVendorById(Integer id){
        Optional<ProdukVendorModel> produkVendor= produkVendorDb.findById(id);
        if(produkVendor.isPresent()){
            return produkVendor.get();
        } else{
            return null;
        }
    }

    @Override
    public void updateProdukVendor(ProdukVendorModel produkVendor){
        produkVendorDb.save(produkVendor);
    }

    @Override
    public void deleteProdukVendor(ProdukVendorModel produkVendor){
        produkVendorDb.delete(produkVendor);
    }

}
