package com.sioffice.service;

import com.sioffice.model.PenggunaModel;
import com.sioffice.repository.PenggunaDb;
import com.sioffice.repository.KaryawanDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.List;

@Service
@Transactional
public class KaryawanServiceImpl implements KaryawanService{
    @Autowired
    private PenggunaDb penggunaDb;
    @Autowired
    private KaryawanDb karyawanDb;

    @Override
    public List<PenggunaModel> listKaryawan(){
        return penggunaDb.findAll();
    }

    @Override
    public void addKaryawan(PenggunaModel karyawan){
        String password = encrypt(karyawan.getPassword());
        karyawan.setPassword(password);
        penggunaDb.save(karyawan);
    }

    @Override
    public String encrypt(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

    @Override
    public PenggunaModel getKaryawanById(Integer id){
        Optional<PenggunaModel> karyawan = karyawanDb.findById(id);
        if(karyawan.isPresent()){
            return karyawan.get();
        } else{
            return null;
        }
    }
    
    @Override
    public void updateKaryawan(PenggunaModel karyawan){
        penggunaDb.save(karyawan);
    }

    @Override
    public void deleteKaryawan(PenggunaModel karyawan){
        penggunaDb.delete(karyawan);
    }
}
