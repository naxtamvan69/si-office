package com.sioffice.service;

import com.sioffice.model.FileSuratKeluarEntity;
import com.sioffice.model.SuratKeluarModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface SuratKeluarService {
    List<SuratKeluarModel> getListSuratKeluar();
    void saveFileSuratKeluar(MultipartFile file, FileSuratKeluarEntity fileSuratKeluar) throws IOException;
    FileSuratKeluarEntity getFileSuratKeluar(String id);
    List<FileSuratKeluarEntity> getListFileSuratKeluar();
    void saveSuratKeluar(SuratKeluarModel suratKeluar);
    void deleteSuratKeluar(SuratKeluarModel suratKeluar);
    SuratKeluarModel getSuratKeluar(Long id);
    void editSuratKeluar(SuratKeluarModel suratKeluar);
    void editFileSuratKeluar(MultipartFile file, FileSuratKeluarEntity fileSuratKeluar) throws IOException;
    List<SuratKeluarModel> getListSuratKeluar(String input);
    List<SuratKeluarModel> getListSuratKeluar(LocalDate input);
    List<SuratKeluarModel> getListSuratKeluarByPageAndSize(int page, int size);
    List<SuratKeluarModel> getListSuratKeluarByPageAndSize(String input, int page, int size);
    List<SuratKeluarModel> getListSuratKeluarByPageAndSize(LocalDate input, int page, int size);
    
}
