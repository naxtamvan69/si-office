package com.sioffice.service;

import com.sioffice.model.NodinModel;
import com.sioffice.model.PenggunaModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface NodinService {
    void saveNodin(NodinModel nodin);
    NodinModel getANotaDinas(long nomor);
    void deleteNotaDinas(NodinModel nodin);
    List<NodinModel> getAllNotaDinas();
    List<NodinModel> getAllNotaDinas(PenggunaModel pengguna);
    List<NodinModel> getAllNotaDinas(int status);
    List<NodinModel> getAllNotaDinas(String input);
    List<NodinModel> getAllNotaDinas(String typeTanggal, LocalDate tanggal);
    List<NodinModel> getAllNotaDinasByPageAndSize(String input, int page, int size);
    List<NodinModel> getAllNotaDinasByPageAndSize(String typeTanggal, LocalDate tanggal, int page, int size);
    List<NodinModel> getAllNotaDinasByPageAndSize(int status, int page, int size);
    List<NodinModel> getAllNotaDinasByPageAndSize(int page, int size);
}
