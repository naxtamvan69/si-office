package com.sioffice.service;

import com.sioffice.model.DivisiModel;
import java.util.List;

public interface DivisiService {
    List<DivisiModel> listDivisi();
}
