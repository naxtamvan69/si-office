package com.sioffice.service;

import com.sioffice.model.RoleModel;
import java.util.List;

public interface RoleService {
    List<RoleModel> getListAllRole();
}
