package com.sioffice.service;

import com.sioffice.model.KompetensiModel;
import com.sioffice.model.SertifikatEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface KompetensiService {
    List<KompetensiModel> listKompetensi();
    void addKompetensi(KompetensiModel kompetensi);
    KompetensiModel getKompetensiById(Integer id);
    void updateKompetensi(KompetensiModel kompetensi);
    void deleteKompetensi(KompetensiModel kompetensi);
    void saveSertifikat(MultipartFile file, SertifikatEntity sertifikat) throws IOException;
    SertifikatEntity getFileSertifikat(String id);
    List<SertifikatEntity> getListFileSertifikat();
}
