package com.sioffice.service;

import com.sioffice.model.PenggunaModel;
import java.util.List;

public interface KaryawanService {
    List<PenggunaModel> listKaryawan();
    void addKaryawan(PenggunaModel karyawan);
    String encrypt(String password);
    PenggunaModel getKaryawanById(Integer id);
    void updateKaryawan(PenggunaModel karyawan);
    void deleteKaryawan(PenggunaModel karyawan);
}
