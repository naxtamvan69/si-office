package com.sioffice.service;

import com.sioffice.model.FaqModel;

import java.util.List;

public interface FaqService {
    List<FaqModel> listFaq();
    void addFaq(FaqModel faq);
    void editFaq(FaqModel faq);
    FaqModel getFaqById(Long id);
    void deleteFaq(FaqModel faq);
    void updatePositionFaq(FaqModel faqDelete);
}
