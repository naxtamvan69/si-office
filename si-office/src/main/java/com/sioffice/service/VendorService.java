package com.sioffice.service;

import com.sioffice.model.VendorModel;

import java.util.List;

public interface VendorService {
    List<VendorModel> listVendor();
    void addVendor(VendorModel vendor);
    VendorModel getVendorById(Integer id);
    void updateVendor(VendorModel vendor);
    void deleteVendor(VendorModel vendor);
    List<VendorModel> getByKeyword(String keyword);
}
