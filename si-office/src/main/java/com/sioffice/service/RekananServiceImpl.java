package com.sioffice.service;

import com.sioffice.model.RekananModel;
import com.sioffice.model.LogoRekananEntity;
import com.sioffice.repository.RekananDb;
import com.sioffice.repository.LogoRekananEntityDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class
RekananServiceImpl implements RekananService{
    @Autowired
    private RekananDb rekananDb;

    @Autowired
    private LogoRekananEntityDb logoRekananEntityDb;

    @Override
    public List<RekananModel> listRekanan(){
        return rekananDb.findAll();
    }

    @Override
    public RekananModel getRekananById(Long id){
        Optional<RekananModel> rekanan = rekananDb.findById(id);
        if(rekanan.isPresent()){
            return rekanan.get();
        } else{
            return null;
        }
    }

    @Override
    public void addRekanan(RekananModel rekanan){
        rekananDb.save(rekanan);
    }

    @Override
    public void saveLogoRekanan(MultipartFile file, LogoRekananEntity logorekanan) throws IOException {
        logorekanan.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        logorekanan.setContentType(file.getContentType());
        logorekanan.setData(file.getBytes());
        logorekanan.setSize(file.getSize());
        
        logoRekananEntityDb.save(logorekanan);
    }

    @Override
    public LogoRekananEntity getLogoRekanan(String id) {
        Optional<LogoRekananEntity> logoRekanan = logoRekananEntityDb.findById(id);
        if(logoRekanan.isPresent()){
            LogoRekananEntity getlogo = (LogoRekananEntity) logoRekanan.get();
            return getlogo;
        } else{
            return null;
        }
    }

    @Override
    public void updateRekanan(RekananModel rekanan){
        rekananDb.save(rekanan);
    }

    @Override
    public void deleteRekanan(RekananModel rekanan){
        rekananDb.delete(rekanan);
    }

    @Override
    public void editRekanan(RekananModel rekanan){
        rekananDb.save(rekanan);
    }


    @Override
    public void editLogoRekanan(MultipartFile file, LogoRekananEntity logorekanan) throws IOException{
        logorekanan.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        logorekanan.setContentType(file.getContentType());
        logorekanan.setData(file.getBytes());
        logorekanan.setSize(file.getSize());

        logoRekananEntityDb.save(logorekanan);
    }
}