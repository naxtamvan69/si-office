package com.sioffice.service;

import com.sioffice.model.FileSuratMasukEntity;
import com.sioffice.model.SuratMasukModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface SuratMasukService {
    List<SuratMasukModel> getListSuratMasuk();
    void saveFileSuratMasuk(MultipartFile file, FileSuratMasukEntity fileSuratMasuk) throws IOException;
    FileSuratMasukEntity getFileSuratMasuk(String id);
    List<FileSuratMasukEntity> getListFileSuratMasuk();
    void addSuratMasuk(SuratMasukModel suratMasuk);
    List<SuratMasukModel> getListSuratMasukByPerihal(String inputPerihal);
    List<SuratMasukModel> getListSuratMasukByPerihalAndPageAndSize(String inputPerihal, int page, int size);
    List<SuratMasukModel> getListSuratMasukByDate(LocalDate inputDate);
    List<SuratMasukModel> getListSuratMasukByDateAndPageAndSize(LocalDate inputDate, int page, int size);
    void deleteSuratMasuk(SuratMasukModel suratMasuk);
    SuratMasukModel getSuratMasuk(Long id);
    void editSuratMasuk(SuratMasukModel suratMasuk);
    void editFileSuratMasuk(MultipartFile file, FileSuratMasukEntity fileSuratMasuk) throws IOException;
    List<SuratMasukModel> getListSuratMasukByPage(int page, int size);
}
