package com.sioffice.service;

import com.sioffice.model.ProdukVendorModel;

import java.util.List;

public interface ProdukVendorService {
    List<ProdukVendorModel> listProdukVendor();
    void addProdukVendor(ProdukVendorModel produkVendor);
    ProdukVendorModel getProdukVendorById(Integer id);
    void updateProdukVendor(ProdukVendorModel produkVendor);
    void deleteProdukVendor(ProdukVendorModel produkVendor);

}
