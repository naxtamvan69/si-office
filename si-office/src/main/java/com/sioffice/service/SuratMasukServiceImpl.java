package com.sioffice.service;

import com.sioffice.model.FileSuratMasukEntity;
import com.sioffice.model.SuratMasukModel;
import com.sioffice.repository.FileSuratMasukEntityDb;
import com.sioffice.repository.SuratMasukDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SuratMasukServiceImpl implements SuratMasukService {
    @Autowired
    private SuratMasukDb suratMasukDb;

    @Autowired
    private FileSuratMasukEntityDb fileSuratMasukEntityDb;

    @Override
    public List<SuratMasukModel> getListSuratMasuk() {
        return suratMasukDb.findAll();
    }

    @Override
    public void saveFileSuratMasuk(MultipartFile file, FileSuratMasukEntity fileSuratMasuk) throws IOException {
        fileSuratMasuk.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        fileSuratMasuk.setContentType(file.getContentType());
        fileSuratMasuk.setData(file.getBytes());
        fileSuratMasuk.setSize(file.getSize());
        fileSuratMasukEntityDb.save(fileSuratMasuk);
    }

    @Override
    public FileSuratMasukEntity getFileSuratMasuk(String id) {
        Optional<FileSuratMasukEntity> fileSuratMasuk = fileSuratMasukEntityDb.findById(id);
        return fileSuratMasuk.orElse(null);
    }

    @Override
    public List<FileSuratMasukEntity> getListFileSuratMasuk() {
        return fileSuratMasukEntityDb.findAll();
    }

    @Override
    public void addSuratMasuk(SuratMasukModel suratMasuk) {
        suratMasukDb.save(suratMasuk);
    }

    @Override
    public List<SuratMasukModel> getListSuratMasukByPerihal(String inputPerihal) {
        List<SuratMasukModel> listSuratMasuk = suratMasukDb.findByFilterPerihal(inputPerihal.toLowerCase());
        return listSuratMasuk;
    }

    @Override
    public List<SuratMasukModel> getListSuratMasukByPerihalAndPageAndSize(String inputPerihal, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("idSuratMasuk").descending());
        Page<SuratMasukModel> pages = suratMasukDb.findByFilterPerihalAndPageAndSize(inputPerihal.toLowerCase(), pageable);
        return pages.getContent();
    }

    @Override
    public List<SuratMasukModel> getListSuratMasukByDate(LocalDate inputDate) {
        List<SuratMasukModel> listSuratMasuk = suratMasukDb.findByFilterDate(inputDate);
        return listSuratMasuk;
    }

    @Override
    public List<SuratMasukModel> getListSuratMasukByDateAndPageAndSize(LocalDate inputDate, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("idSuratMasuk").descending());
        Page<SuratMasukModel> pages = suratMasukDb.findByFilterTanggalAndPageAndSize(inputDate, pageable);
        return pages.getContent();
    }

    @Override
    public void deleteSuratMasuk(SuratMasukModel suratMasuk) {
        suratMasukDb.delete(suratMasuk);
    }

    @Override
    public SuratMasukModel getSuratMasuk(Long id) {
        Optional<SuratMasukModel> suratMasuk = suratMasukDb.findByIdSuratMasuk(id);
        return suratMasuk.orElse(null);
    }

    @Override
    public void editSuratMasuk(SuratMasukModel suratMasuk) {
        suratMasukDb.save(suratMasuk);
    }

    @Override
    public void editFileSuratMasuk(MultipartFile file, FileSuratMasukEntity fileSuratMasuk) throws IOException {
        fileSuratMasuk.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        fileSuratMasuk.setContentType(file.getContentType());
        fileSuratMasuk.setData(file.getBytes());
        fileSuratMasuk.setSize(file.getSize());
        fileSuratMasukEntityDb.save(fileSuratMasuk);
    }

    @Override
    public List<SuratMasukModel> getListSuratMasukByPage(int page, int size) {
        Page<SuratMasukModel> pas = suratMasukDb.findAll(PageRequest.of(page, size, Sort.by("idSuratMasuk").descending()));
        return pas.getContent();
    }
}
