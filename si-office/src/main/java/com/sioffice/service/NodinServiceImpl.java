package com.sioffice.service;

import com.sioffice.model.NodinModel;
import com.sioffice.model.PenggunaModel;
import com.sioffice.repository.NodinDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class NodinServiceImpl implements NodinService {
    @Autowired
    NodinDb nodinDb;

    @Override
    public List<NodinModel> getAllNotaDinas(PenggunaModel pengguna) {
        return nodinDb.findAllByPenggunaOrderByNomorDesc(pengguna);
    }

    @Override
    public void saveNodin(NodinModel nodin) {
        nodinDb.save(nodin);
    }

    @Override
    public NodinModel getANotaDinas(long nomor) {
        return nodinDb.getById(nomor);
    }

    @Override
    public void deleteNotaDinas(NodinModel nodin) {
        nodinDb.delete(nodin);
    }

    @Override
    public List<NodinModel> getAllNotaDinas(int status) {
        return nodinDb.findAllByStatusOrderByNomorDesc(status);
    }

    @Override
    public List<NodinModel> getAllNotaDinas() {
        return nodinDb.findAllDisetujiAndTidakDisetuji();
    }

    @Override
    public List<NodinModel> getAllNotaDinas(String input) {
        return nodinDb.findAllByPerihal(input.toLowerCase());
    }

    @Override
    public List<NodinModel> getAllNotaDinas(String typeTanggal, LocalDate tanggal) {
        List<NodinModel> listNodin = new ArrayList<>();
        if (typeTanggal.equals("tanggal-pengajuan")) {
            listNodin = nodinDb.findAllByTanggalPengajuan(tanggal);
        } else if (typeTanggal.equals("tanggal-persetujuan-manajer")) {
            listNodin = nodinDb.findAllByTanggalPersetujuanManajer(tanggal);
        }
        System.out.println(listNodin);
        return listNodin;
    }

    @Override
    public List<NodinModel> getAllNotaDinasByPageAndSize(String input, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("tanggalPengajuan").descending());
        return nodinDb.findAllByPerihalAndPageAndSize(input.toLowerCase(), pageable).getContent();
    }

    @Override
    public List<NodinModel> getAllNotaDinasByPageAndSize(String typeTanggal, LocalDate tanggal, int page, int size) {
        List<NodinModel> listNodin = new ArrayList<>();
        if (typeTanggal.equals("tanggal-pengajuan")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by("tanggalPengajuan").descending());
            listNodin = nodinDb.findAllByTanggalPengajuanAndPageAndSize(tanggal, pageable).getContent();
        } else if (typeTanggal.equals("tanggal-persetujuan-manajer")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by("tanggalPengajuan").descending());
            listNodin = nodinDb.findAllByTanggalPersetujuanManajerAndPageAndSize(tanggal, pageable).getContent();
        }
        return listNodin;
    }

    @Override
    public List<NodinModel> getAllNotaDinasByPageAndSize(int status, int page, int size) {
        List<NodinModel> listNodin = new ArrayList<>();
        if (status == 4) {
            Pageable pageable = PageRequest.of(page, size, Sort.by("tanggalPengajuan").descending());
            listNodin = nodinDb.findAllByDisetujuiDirekturAndPageAndSize(pageable).getContent();
        } else if (status == 5) {
            Pageable pageable = PageRequest.of(page, size, Sort.by("tanggalPengajuan").descending());
            listNodin = nodinDb.findAllByDitolakDirekturAndPageAndSize(pageable).getContent();
        }
        return listNodin;
    }

    @Override
    public List<NodinModel> getAllNotaDinasByPageAndSize(int page, int size) {
        return nodinDb.findAllDisetujiAndTidakDisetujiByPageAndSize(PageRequest.of(page, size, Sort.by("tanggalPengajuan").descending())).getContent();
    }
}

