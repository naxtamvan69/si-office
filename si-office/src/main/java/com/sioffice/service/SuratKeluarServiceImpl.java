package com.sioffice.service;

import com.sioffice.model.FileSuratKeluarEntity;
import com.sioffice.model.SuratKeluarModel;
import com.sioffice.repository.FileSuratKeluarEntityDb;
import com.sioffice.repository.SuratKeluarDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SuratKeluarServiceImpl implements SuratKeluarService{
    @Autowired
    private SuratKeluarDb suratKeluarDb;

    @Autowired
    private FileSuratKeluarEntityDb fileSuratKeluarEntityDb;

    @Override
    public List<SuratKeluarModel> getListSuratKeluar() {
        return suratKeluarDb.findAll();
    }

    @Override
    public void saveFileSuratKeluar(MultipartFile file, FileSuratKeluarEntity fileSuratKeluar) throws IOException {
        fileSuratKeluar.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        fileSuratKeluar.setContentType(file.getContentType());
        fileSuratKeluar.setData(file.getBytes());
        fileSuratKeluar.setSize(file.getSize());
        fileSuratKeluarEntityDb.save(fileSuratKeluar);
    }

    @Override
    public FileSuratKeluarEntity getFileSuratKeluar(String id) {
        Optional<FileSuratKeluarEntity> fileSuratKeluar = fileSuratKeluarEntityDb.findById(id);
        return fileSuratKeluar.orElse(null);
    }

    @Override
    public List<FileSuratKeluarEntity> getListFileSuratKeluar() {
        return fileSuratKeluarEntityDb.findAll();
    }

    @Override
    public void saveSuratKeluar(SuratKeluarModel suratKeluar) {
        suratKeluarDb.save(suratKeluar);
    }

    @Override
    public void deleteSuratKeluar(SuratKeluarModel suratKeluar) {
        suratKeluarDb.delete(suratKeluar);
    }

    @Override
    public SuratKeluarModel getSuratKeluar(Long id) {
        Optional<SuratKeluarModel> suratKeluar = Optional.of(suratKeluarDb.getById(id));
        return suratKeluar.orElse(null);
    }

    @Override
    public void editSuratKeluar(SuratKeluarModel suratKeluar) {
        suratKeluarDb.save(suratKeluar);
    }

    @Override
    public void editFileSuratKeluar(MultipartFile file, FileSuratKeluarEntity fileSuratKeluar) throws IOException{
        fileSuratKeluar.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        fileSuratKeluar.setContentType(file.getContentType());
        fileSuratKeluar.setData(file.getBytes());
        fileSuratKeluar.setSize(file.getSize());
        fileSuratKeluarEntityDb.save(fileSuratKeluar);
    }

    @Override
    public List<SuratKeluarModel> getListSuratKeluar(String input) {
        return suratKeluarDb.findAllByPerihal(input.toLowerCase());
    }

    @Override
    public List<SuratKeluarModel> getListSuratKeluar(LocalDate input) {
        return suratKeluarDb.findAllByDate(input);
    }

    @Override
    public List<SuratKeluarModel> getListSuratKeluarByPageAndSize(int page, int size) {
        return suratKeluarDb.findAll(PageRequest.of(page, size, Sort.by("tanggal").descending())).getContent();
    }

    @Override
    public List<SuratKeluarModel> getListSuratKeluarByPageAndSize(String input, int page, int size) {
        return suratKeluarDb.findAllByPerihalAndPageAndSize(input.toLowerCase(), PageRequest.of(page, size, Sort.by("tanggal").descending())).getContent();
    }

    @Override
    public List<SuratKeluarModel> getListSuratKeluarByPageAndSize(LocalDate input, int page, int size) {
        return suratKeluarDb.findAllByDateAndPageAndSize(input, PageRequest.of(page, size, Sort.by("tanggal").descending())).getContent();
    }
}
