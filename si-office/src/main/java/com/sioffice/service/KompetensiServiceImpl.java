package com.sioffice.service;

import com.sioffice.model.IjazahEntity;
import com.sioffice.model.KompetensiModel;
import com.sioffice.model.RiwayatPendidikan;
import com.sioffice.model.SertifikatEntity;
import com.sioffice.repository.KompetensiDb;
import com.sioffice.repository.SertifikatDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class KompetensiServiceImpl implements KompetensiService {
    @Autowired
    private KompetensiDb kompetensiDb;

    @Autowired
    private SertifikatDb sertifikatDb;

    @Override
    public List<KompetensiModel> listKompetensi() {return kompetensiDb.findAll();}

    @Override
    public void addKompetensi(KompetensiModel kompetensi) {
        kompetensiDb.save(kompetensi);
    }

    @Override
    public KompetensiModel getKompetensiById(Integer id) {
        Optional<KompetensiModel> kompetensi = kompetensiDb.findById(id);
        if(kompetensi.isPresent()){
            return kompetensi.get();
        } else{
            return null;
        }
    }

    @Override
    public void updateKompetensi(KompetensiModel kompetensi) {
        kompetensiDb.save(kompetensi);
    }

    @Override
    public void deleteKompetensi(KompetensiModel kompetensi) {

        kompetensiDb.delete(kompetensi);
    }

    @Override
    public void saveSertifikat(MultipartFile file, SertifikatEntity sertifikat) throws IOException {
        sertifikat.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        sertifikat.setContentType(file.getContentType());
        sertifikat.setData(file.getBytes());
        sertifikat.setSize(file.getSize());
        sertifikatDb.save(sertifikat);
    }

    @Override
    public SertifikatEntity getFileSertifikat(String id) {
        Optional<SertifikatEntity> sertifikatFile = sertifikatDb.findById(id);
        return sertifikatFile.orElse(null);
    }

    @Override
    public List<SertifikatEntity> getListFileSertifikat() {
        return sertifikatDb.findAll();
    }

}
