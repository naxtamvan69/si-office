package com.sioffice.service;

import com.sioffice.model.FaqModel;
import com.sioffice.repository.FaqDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FaqServiceImpl implements FaqService {
    @Autowired
    private FaqDb faqDb;

    @Override
    public List<FaqModel> listFaq() {
        return faqDb.findAll(Sort.by(Sort.Direction.ASC, "noUrutan"));
    }

    @Override
    public void addFaq(FaqModel faq) {
        faqDb.save(faq);
    }

    @Override
    public void editFaq(FaqModel faq) {
        faqDb.save(faq);
    }

    @Override
    public FaqModel getFaqById(Long id) {
        Optional<FaqModel> faq = faqDb.findByIdFaq(id);
        return faq.orElse(null);
    }

    @Override
    public void deleteFaq(FaqModel faq) {
        faqDb.delete(faq);
    }

    @Override
    public void updatePositionFaq(FaqModel faqDelete) {
        List<FaqModel> daftarFaq = listFaq();
        for (int i = 1; i < daftarFaq.size() + 1; i++) {
            if (i >= faqDelete.getNoUrutan()) {
                FaqModel faq = daftarFaq.get(i - 1);
                faq.setNoUrutan(i);
                editFaq(faq);
            }
        }
    }
}
