package com.sioffice.service;

import com.sioffice.model.RekananModel;
import com.sioffice.model.LogoRekananEntity;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

public interface RekananService {
    List<RekananModel> listRekanan();
    void addRekanan(RekananModel rekanan);
    RekananModel getRekananById(Long id);
    void saveLogoRekanan(MultipartFile file, LogoRekananEntity logorekanan) throws IOException;
    LogoRekananEntity getLogoRekanan(String id);
    void updateRekanan(RekananModel rekanan);
    void deleteRekanan(RekananModel rekanan);
    void editRekanan(RekananModel rekanan);
    void editLogoRekanan(MultipartFile file, LogoRekananEntity logorekanan) throws IOException;
}
