package com.sioffice.service;

import com.sioffice.model.KeluargaModel;
import com.sioffice.model.KompetensiModel;
import com.sioffice.repository.KeluargaDb;
import com.sioffice.repository.KompetensiDb;
import com.sioffice.repository.PenggunaDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class KeluargaServiceImpl implements KeluargaService{

    @Autowired
    private PenggunaDb penggunaDb;

    @Autowired
    private KeluargaDb keluargaDb;

    @Override
    public List<KeluargaModel> listKeluarga() {
         return keluargaDb.findAll();
    }

    @Override
    public void addKeluarga(KeluargaModel keluarga){
        keluargaDb.save(keluarga);
    }

    @Override
    public KeluargaModel getKeluargaById(Integer id) {
        Optional<KeluargaModel> keluarga = keluargaDb.findById(id);
        if(keluarga.isPresent()){
            return keluarga.get();
        } else{
            return null;
        }
    }

    @Override
    public void updateKeluarga(KeluargaModel keluarga) {
        keluargaDb.save(keluarga);
    }

    @Override
    public void deleteKeluarga(KeluargaModel keluarga){
        keluargaDb.delete(keluarga);
    }
}
