package com.sioffice.service;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sioffice.model.RoleModel;
import com.sioffice.repository.RoleDb;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDb roleDb;

    @Override
    public List<RoleModel> getListAllRole() {
        return roleDb.findAll();
    }
}
