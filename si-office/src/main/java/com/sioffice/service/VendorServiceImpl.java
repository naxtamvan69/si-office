package com.sioffice.service;

import com.sioffice.model.VendorModel;
import com.sioffice.repository.ProdukVendorDb;
import com.sioffice.repository.VendorDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class VendorServiceImpl implements VendorService {
    @Autowired
    private VendorDb vendorDb;
    @Autowired
    private ProdukVendorDb produkVendorDb;

    @Override
    public List<VendorModel> listVendor(){
        return vendorDb.findAll();
    }

    @Override
    public void addVendor(VendorModel vendor){
        vendorDb.save(vendor);
    }

    @Override
    public VendorModel getVendorById(Integer id){
        Optional<VendorModel> vendor = vendorDb.findById(id);
        if(vendor.isPresent()){
            return vendor.get();
        } else{
            return null;
        }
    }

    @Override
    public void updateVendor(VendorModel vendor){
        vendorDb.save(vendor);
    }

    @Override
    public void deleteVendor(VendorModel vendor){
        vendorDb.delete(vendor);
    }

    public List<VendorModel> getByKeyword(String keyword){
        return vendorDb.findByKeyword(keyword);
    }
}
