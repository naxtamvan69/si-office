package com.sioffice.service;

import com.sioffice.model.CutiModel;
import com.sioffice.model.PenggunaModel;
import com.sioffice.repository.CutiDb;
import com.sioffice.repository.KaryawanDb;
import com.sioffice.repository.PenggunaDb;
import com.sioffice.service.CutiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Transactional
public class CutiServiceImpl implements CutiService {
    @Autowired
    private CutiDb cutiDb;

    @Autowired
    private PenggunaDb penggunaDb;

    @Override
    public List<CutiModel> listCuti(){
        return cutiDb.findAll();
    }

    @Override
    public void addCuti(CutiModel cuti){
        cutiDb.save(cuti);
    }

    @Override
    public CutiModel getCutiByNoCuti(long noCuti){
        Optional<CutiModel> cuti = cutiDb.findByNoCuti(noCuti);
        if(cuti.isPresent()){
            return cuti.get();
        } else{
            return null;
        }
    }

    @Override
    public List<LocalDate> generateDates(
            LocalDate startDate, LocalDate endDate) {

        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween+1)
                .mapToObj(i -> startDate.plusDays(i))
                .collect(Collectors.toList());
    }

    @Override
    public void save(CutiModel cuti){
        cutiDb.save(cuti);
    }

    @Override
    public int workingDays(LocalDate startDate, LocalDate endDate){
        //conversion
        ZoneId defaultZoneId = ZoneId.systemDefault();
        Date start = Date.from(startDate.atStartOfDay(defaultZoneId).toInstant());
        Date end = Date.from(endDate.atStartOfDay(defaultZoneId).toInstant());

        //initiation
        int count = 0;
        Date curDate = new Date(start.getTime());

        //add 1 day for iterations
        Calendar c = Calendar.getInstance();
        c.setTime(end);
        c.add(Calendar.DATE, 1);
        end = c.getTime();

        //process
        while(curDate.before(end)){
            int dayOfWeek = curDate.getDay();
            if(dayOfWeek != 0 && dayOfWeek != 6) {
                count++;
            }
            curDate.setDate(curDate.getDate() + 1);
        }
        return count;
    }}
