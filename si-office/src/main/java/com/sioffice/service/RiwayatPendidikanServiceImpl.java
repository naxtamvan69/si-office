package com.sioffice.service;

import com.sioffice.model.*;
import com.sioffice.repository.IjazahDb;
import com.sioffice.repository.RiwayatPendidikanDb;
import com.sioffice.repository.PenggunaDb;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class RiwayatPendidikanServiceImpl implements RiwayatPendidikanService{
    @Autowired
    private RiwayatPendidikanDb riwayatPendidikanDb;

    @Autowired
    private IjazahDb ijazahDb;

    @Override
    public List<RiwayatPendidikan> listRiwayatPendidikan() {
        return riwayatPendidikanDb.findAll();
    }

    @Override
    public void addPendidikan(RiwayatPendidikan pendidikan) {
        riwayatPendidikanDb.save(pendidikan);
    }

    @Override
    public RiwayatPendidikan getPendidikanById(Integer id) {
        Optional<RiwayatPendidikan> pendidikan = riwayatPendidikanDb.findById(id);
        if(pendidikan.isPresent()){
            return pendidikan.get();
        } else{
            return null;
        }
    }

    @Override
    public void updatePendidikan(RiwayatPendidikan pendidikan) {
        riwayatPendidikanDb.save(pendidikan);
    }

    @Override
    public void deletePendidikan(RiwayatPendidikan pendidikan) {
        riwayatPendidikanDb.delete(pendidikan);
    }

    @Override
    public void saveIjazah(MultipartFile file, IjazahEntity ijazah) throws IOException {
        ijazah.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        ijazah.setContentType(file.getContentType());
        ijazah.setData(file.getBytes());
        ijazah.setSize(file.getSize());
        ijazahDb.save(ijazah);
    }

    @Override
    public IjazahEntity getFileIjazah(String id) {
        Optional<IjazahEntity> ijazahFile = ijazahDb.findById(id);
        return ijazahFile.orElse(null);
    }

    @Override
    public List<IjazahEntity> getListFileIjazah() {
        return ijazahDb.findAll();
    }
}
