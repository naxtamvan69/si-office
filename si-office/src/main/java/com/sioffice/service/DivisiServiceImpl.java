package com.sioffice.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sioffice.model.DivisiModel;
import com.sioffice.repository.DivisiDb;

@Service
@Transactional
public class DivisiServiceImpl implements DivisiService{
    @Autowired
    private DivisiDb divisiDb;

    @Override
    public List<DivisiModel> listDivisi(){
        return divisiDb.findAll();
    }
}
