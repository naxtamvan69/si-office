package com.sioffice.service;

import com.sioffice.model.FileTtdEntity;
import com.sioffice.model.FotoEntity;
import com.sioffice.model.PenggunaModel;
import com.sioffice.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PenggunaServiceImpl implements PenggunaService {
    @Autowired
    private PenggunaDb penggunaDb;

    @Autowired
    private KompetensiDb kompetensiDb;

    @Autowired
    private FileTtdEntityDb fileTtdEntityDb;

    @Autowired
    private FotoDb fotoDb;

    @Autowired
    private PasswordRepository passwordRepository;

    @Override
    public PenggunaModel save(PenggunaModel pengguna) {
        penggunaDb.save(pengguna);
        return pengguna;
    }

    @Override
    public void updateProfil(PenggunaModel pengguna) {
        penggunaDb.save(pengguna);
    }

    @Override
    public List<PenggunaModel> getListUser() {
        return penggunaDb.findAll();
    }

    @Override
    public PenggunaModel getPenggunaByUsername(String username) {
        PenggunaModel pengguna = penggunaDb.findByUsername(username);
        return pengguna;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        exposeDirectory("user-ttd", registry);
    }

    private void exposeDirectory(String dirName, ResourceHandlerRegistry registry) {
        Path uploadDir = Paths.get(dirName);
        String uploadPath = uploadDir.toFile().getAbsolutePath();

        if (dirName.startsWith("../")) dirName = dirName.replace("../", "");

        registry.addResourceHandler("/" + dirName + "/**").addResourceLocations("file:/"+ uploadPath + "/");
    }

    @Override
     public String encryptPassword(String password) {
         BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
         String passwordHash = passwordEncoder.encode(password);
         return passwordHash;
     }

     @Override
     public void saveTtdPengguna(MultipartFile file, FileTtdEntity TtdPengguna) throws IOException {
         TtdPengguna.setName(StringUtils.cleanPath(file.getOriginalFilename()));
         TtdPengguna.setContentType(file.getContentType());
         TtdPengguna.setData(file.getBytes());
         TtdPengguna.setSize(file.getSize());

         fileTtdEntityDb.save(TtdPengguna);
     }

     @Override
     public FileTtdEntity getImages(String id) {
        Optional findById = fileTtdEntityDb.findById(id);
        if (findById.isPresent()) {
            FileTtdEntity getImage = (FileTtdEntity) findById.get();
            return getImage;
        } else {
            return null;
        }
    }

    @Override
    public Integer jml_tanggungan(PenggunaModel pengguna) {
        if (pengguna.getListKeluarga().size() != 0) {
            return pengguna.getListKeluarga().size();
        }
        return 0;
    }

    @Override
    public Integer hitung_umur(PenggunaModel pengguna) {
        Calendar kalendar = Calendar.getInstance();
        int tahun_sekarang = kalendar.get(Calendar.YEAR);
        int tahun_lahir = pengguna.getTanggal_lahir().getYear();
        return tahun_sekarang - tahun_lahir;
    }

    @Override
    public void saveFotoPengguna(MultipartFile file, FotoEntity FotoPengguna) throws IOException {
        FotoPengguna.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        FotoPengguna.setContentType(file.getContentType());
        FotoPengguna.setData(file.getBytes());
        FotoPengguna.setSize(file.getSize());

        fotoDb.save(FotoPengguna);
    }

    @Override
    public FotoEntity getFoto(String id) {
        Optional findById = fotoDb.findById(id);
        if (findById.isPresent()) {
            FotoEntity getFoto = (FotoEntity) findById.get();
            return getFoto;
        } else {
            return null;
        }
    }

    @Override
    public Boolean validatePassword(PenggunaModel pengguna, String passwordLama) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        Boolean validasi = passwordEncoder.matches(passwordLama, pengguna.getPassword());
        return validasi;
    }

    @Override
    public PenggunaModel updatePassword(PenggunaModel pengguna) {
        String pass = encryptPassword(pengguna.getPassword());
        pengguna.setPassword(pass);
        return penggunaDb.save(pengguna);
    }

    public void updateResetPasswordToken(String token, String email) {
        PenggunaModel pengguna = passwordRepository.findByEmail(email);
        if (pengguna != null) {
            pengguna.setResetPasswordToken(token);
            passwordRepository.save(pengguna);
        }
    }

    public PenggunaModel getByResetPasswordToken(String token) {
        return passwordRepository.findByResetPasswordToken(token);
    }

    public void updatePassword(PenggunaModel pengguna, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        pengguna.setPassword(encodedPassword);

        pengguna.setResetPasswordToken(null);
        passwordRepository.save(pengguna);
    }

    public Integer checkEmail(String email) {
        int check = 0;

        PenggunaModel validateEmail = penggunaDb.findByEmail(email);
        if (validateEmail != null) {
            check = 1;
        }
        return check;
    }
}
