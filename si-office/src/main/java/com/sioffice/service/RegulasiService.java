package com.sioffice.service;

import com.sioffice.model.FileRegulasiEntity;
import com.sioffice.model.RegulasiModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface RegulasiService {
    List<RegulasiModel> getlistRegulasi();
    void saveFileRegulasi(MultipartFile file, FileRegulasiEntity fileRegulasi) throws IOException;
    FileRegulasiEntity getFileRegulasi(String id);
    List<FileRegulasiEntity> getListFileRegulasi();
    void addRegulasi(RegulasiModel regulasi);
    List<RegulasiModel> getListRegulasiByPerihal(String inputPerihal);
    List<RegulasiModel> getListRegulasiByDate(LocalDate inputDate);
    void deleteRegulsi(RegulasiModel regulasi);
    RegulasiModel getRegulasi(Long id);
    MultipartFile convertFileRegulasiToMultiPart(FileRegulasiEntity fileRegulasi);
    void editRegulasi(RegulasiModel regulasi);
    void editFileRegulasi(MultipartFile file, FileRegulasiEntity fileRegulasi) throws IOException;
    List<RegulasiModel> getListRegulasiByPerihalAndPageAndSize(String inputPerihal, int page, int size);
    List<RegulasiModel> getListRegulasiByDateAndPageAndSize(LocalDate inputTanggal, int page, int size);
    List<RegulasiModel> getListRegulasiByPageAndSize(int page, int size);
}
