package com.sioffice.service;

import com.sioffice.model.IjazahEntity;
import com.sioffice.model.PenggunaModel;
import com.sioffice.model.RiwayatPendidikan;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public interface RiwayatPendidikanService {
    List<RiwayatPendidikan> listRiwayatPendidikan();
    void addPendidikan(RiwayatPendidikan pendidikan);
    RiwayatPendidikan getPendidikanById(Integer id);
    void updatePendidikan(RiwayatPendidikan pendidikan);
    void deletePendidikan(RiwayatPendidikan pendidikan);
    void saveIjazah(MultipartFile file, IjazahEntity ijazah) throws IOException;
    IjazahEntity getFileIjazah(String id);
    List<IjazahEntity> getListFileIjazah();
}
