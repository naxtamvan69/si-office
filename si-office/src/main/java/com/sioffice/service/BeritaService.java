package com.sioffice.service;

import com.sioffice.model.BeritaModel;
import com.sioffice.model.PenggunaModel;

import java.time.LocalDate;
import java.util.List;

public interface BeritaService {
    List<BeritaModel> getListBerita();
    void addBerita(BeritaModel berita);
    BeritaModel getBeritaByNoBerita(long noBerita);
    void save(BeritaModel berita);
    void delete(BeritaModel berita);
}
