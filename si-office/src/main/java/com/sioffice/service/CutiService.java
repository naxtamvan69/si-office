package com.sioffice.service;

import com.sioffice.model.CutiModel;
import com.sioffice.model.PenggunaModel;

import java.time.LocalDate;
import java.util.List;

public interface CutiService {
    List<CutiModel> listCuti();
    void addCuti(CutiModel cuti);
    CutiModel getCutiByNoCuti(long noCuti);
    void save(CutiModel cuti);
    List<LocalDate> generateDates(LocalDate startDate, LocalDate endDate);
    int workingDays(LocalDate startDate, LocalDate endDate);
}
