package com.sioffice.service;

import com.sioffice.model.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import java.io.IOException;
import java.util.*;
public interface PenggunaService {
    List<PenggunaModel> getListUser();
    PenggunaModel save(PenggunaModel pengguna);
    void updateProfil(PenggunaModel pengguna);
    PenggunaModel getPenggunaByUsername(String username);
    void addResourceHandlers(ResourceHandlerRegistry registry);
    String encryptPassword(String password);
    void saveTtdPengguna(MultipartFile file, FileTtdEntity TtdPengguna) throws IOException;
    FileTtdEntity getImages(String id);
    Integer jml_tanggungan(PenggunaModel pengguna);
    Integer hitung_umur(PenggunaModel pengguna);
    void saveFotoPengguna(MultipartFile file, FotoEntity FotoPengguna) throws IOException;
    FotoEntity getFoto(String id);
    Boolean validatePassword(PenggunaModel pengguna, String passwordLama);
    PenggunaModel updatePassword(PenggunaModel pengguna);
    void updateResetPasswordToken(String token, String email);
    PenggunaModel getByResetPasswordToken(String token);
    void updatePassword(PenggunaModel pengguna, String newPassword);
    Integer checkEmail(String email);
}
