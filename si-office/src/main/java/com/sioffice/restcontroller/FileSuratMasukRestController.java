package com.sioffice.restcontroller;

import com.sioffice.model.FileRegulasiEntity;
import com.sioffice.model.FileSuratMasukEntity;
import com.sioffice.model.SuratMasukModel;
import com.sioffice.rest.FileResponse;
import com.sioffice.service.SuratMasukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/file-surat-masuk")
public class FileSuratMasukRestController {
    @Qualifier("suratMasukServiceImpl")
    @Autowired
    private SuratMasukService suratMasukService;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFileSuratMasuk(@RequestParam("file") MultipartFile file) {
        try {
            FileSuratMasukEntity fileSuratMasuk = new FileSuratMasukEntity();
            suratMasukService.saveFileSuratMasuk(file, fileSuratMasuk);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(String.format("File uploaded successfully: %s", file.getOriginalFilename()));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(String.format("Could not upload the file: %s!", file.getOriginalFilename()));
        }
    }

    @GetMapping("")
    public List<FileResponse> daftarFileSuratMasuk() {
        return suratMasukService.getListFileSuratMasuk()
                .stream()
                .map(this::mapToFileResponse)
                .collect(Collectors.toList());
    }

    private FileResponse mapToFileResponse(FileSuratMasukEntity fileSuratMasuk) {
        String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/file-suratMasuk/")
                .path(fileSuratMasuk.getId())
                .toUriString();

        FileResponse file = new FileResponse();
        file.setId(fileSuratMasuk.getId());
        file.setName(fileSuratMasuk.getName());
        file.setContentType(fileSuratMasuk.getContentType());
        file.setSize(fileSuratMasuk.getSize());
        file.setUrl(downloadURL);
        return file;
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getFileSuratMasuk(@PathVariable String id) {
        FileSuratMasukEntity fileSuratMasuk = suratMasukService.getFileSuratMasuk(id);

        if (fileSuratMasuk==null) {
            return ResponseEntity.notFound()
                    .build();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileSuratMasuk.getName() + "\"")
                .contentType(MediaType.valueOf(fileSuratMasuk.getContentType()))
                .body(fileSuratMasuk.getData());
    }
}
