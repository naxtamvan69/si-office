package com.sioffice.restcontroller;

import com.sioffice.model.FaqModel;
import com.sioffice.rest.BaseResponse;
import com.sioffice.rest.ListUbahUrutanFaqDTO;
import com.sioffice.rest.UbahUrutanFaqDTO;
import com.sioffice.service.FaqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/faq")
public class FaqRestController {
    @Qualifier("faqServiceImpl")
    @Autowired
    FaqService faqService;

    @GetMapping("")
    private BaseResponse<List<FaqModel>> getAllFaq() {
        BaseResponse<List<FaqModel>> response = new BaseResponse<>();
        response.setStatus(200);
        response.setMessage("success");
        response.setResult(faqService.listFaq());
        return response;
    }

    @PostMapping("")
    private BaseResponse<String> ubahUrutanFaq(@RequestBody ListUbahUrutanFaqDTO data) {
        for (UbahUrutanFaqDTO urutanFaq : data.getData()) {
            FaqModel faq = faqService.getFaqById(urutanFaq.getId());
            faq.setNoUrutan(urutanFaq.getUrutanUpdate());
            faqService.editFaq(faq);
        }
        BaseResponse<String> response = new BaseResponse<>();
        response.setStatus(200);
        response.setMessage("success");
        response.setResult("yeeee");
        return response;
    }

}
