package com.sioffice.restcontroller;

import com.sioffice.model.FileRegulasiEntity;
import com.sioffice.model.IjazahEntity;
import com.sioffice.rest.FileResponse;
import com.sioffice.service.RiwayatPendidikanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/file-ijazah")
public class IjazahRestController {
    @Qualifier("riwayatPendidikanServiceImpl")
    @Autowired
    private RiwayatPendidikanService riwayatPendidikanService;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFileIjazah(@RequestParam("file")MultipartFile file) {
        try {
            IjazahEntity ijazahFile = new IjazahEntity();
            riwayatPendidikanService.saveIjazah(file, ijazahFile);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(String.format("File uploaded successfully: %s", file.getOriginalFilename()));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(String.format("Could not upload the file: %s!", file.getOriginalFilename()));
        }
    }

    @GetMapping("")
    public List<FileResponse> daftarFileIjazah() {
        return riwayatPendidikanService.getListFileIjazah()
                .stream()
                .map(this::mapToFileResponse)
                .collect(Collectors.toList());
    }

    private FileResponse mapToFileResponse(IjazahEntity ijazahFile) {
        String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/file-ijazah/")
                .path(ijazahFile.getId())
                .toUriString();

        FileResponse file = new FileResponse();
        file.setId(ijazahFile.getId());
        file.setName(ijazahFile.getName());
        file.setContentType(ijazahFile.getContentType());
        file.setSize(ijazahFile.getSize());
        file.setUrl(downloadURL);
        return file;
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getIjazahFile(@PathVariable String id) {
        IjazahEntity ijazah = riwayatPendidikanService.getFileIjazah(id);

        if (ijazah==null) {
            return ResponseEntity.notFound()
                    .build();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + ijazah.getName() + "\"")
                .contentType(MediaType.valueOf(ijazah.getContentType()))
                .body(ijazah.getData());
    }
}
