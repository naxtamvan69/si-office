package com.sioffice.restcontroller;

import com.sioffice.model.RegulasiModel;
import com.sioffice.model.SuratMasukModel;
import com.sioffice.rest.BaseResponse;
import com.sioffice.service.RegulasiService;
import com.sioffice.service.SuratMasukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/regulasi")
public class RegulasiRestController {
    @Qualifier("regulasiServiceImpl")
    @Autowired
    RegulasiService regulasiService;

    @PostMapping("/delete/{id}")
    private BaseResponse<String> deleteRegulasi(@PathVariable Long id) {
        BaseResponse<String> response = new BaseResponse<>();
        try {
            RegulasiModel regulasi = regulasiService.getRegulasi(id);
            if (regulasi != null) {
                regulasiService.deleteRegulsi(regulasi);
                response.setStatus(200);
                response.setMessage("success");
                response.setResult("Regulasi berhasil dihapus");
            } else {
                throw new Exception("Regulasi tidak ada di database");
            }
        } catch (Exception e) {
            response.setStatus(500);
            response.setMessage("failed");
            response.setResult(e.getMessage());
        }
        return response;

    }
}
