package com.sioffice.restcontroller;

import com.sioffice.model.FileSuratKeluarEntity;
import com.sioffice.model.FileSuratMasukEntity;
import com.sioffice.rest.FileResponse;
import com.sioffice.service.SuratKeluarService;
import com.sioffice.service.SuratMasukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/file-surat-keluar")
public class FileSuratKeluarRestController {
    @Qualifier("suratKeluarServiceImpl")
    @Autowired
    private SuratKeluarService suratKeluarService;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFileSuratKeluar(@RequestParam("file") MultipartFile file) {
        try {
            FileSuratKeluarEntity fileSuratKeluar = new FileSuratKeluarEntity();
            suratKeluarService.saveFileSuratKeluar(file, fileSuratKeluar);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(String.format("File uploaded successfully: %s", file.getOriginalFilename()));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(String.format("Could not upload the file: %s!", file.getOriginalFilename()));
        }
    }

    @GetMapping("")
    public List<FileResponse> daftarFileSuratKeluar() {
        return suratKeluarService.getListFileSuratKeluar()
                .stream()
                .map(this::mapToFileResponse)
                .collect(Collectors.toList());
    }

    private FileResponse mapToFileResponse(FileSuratKeluarEntity fileSuratKeluar) {
        String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/file-surat-Keluar/")
                .path(fileSuratKeluar.getId())
                .toUriString();

        FileResponse file = new FileResponse();
        file.setId(fileSuratKeluar.getId());
        file.setName(fileSuratKeluar.getName());
        file.setContentType(fileSuratKeluar.getContentType());
        file.setSize(fileSuratKeluar.getSize());
        file.setUrl(downloadURL);
        return file;
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getFileSuratKeluar(@PathVariable String id) {
        FileSuratKeluarEntity fileSuratKeluar = suratKeluarService.getFileSuratKeluar(id);

        if (fileSuratKeluar==null) {
            return ResponseEntity.notFound()
                    .build();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileSuratKeluar.getName() + "\"")
                .contentType(MediaType.valueOf(fileSuratKeluar.getContentType()))
                .body(fileSuratKeluar.getData());
    }
}
