package com.sioffice.restcontroller;

import com.sioffice.model.SuratMasukModel;
import com.sioffice.rest.BaseResponse;
import com.sioffice.service.SuratMasukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/surat-masuk")
public class SuratMasukRestController {
    @Qualifier("suratMasukServiceImpl")
    @Autowired
    SuratMasukService suratMasukService;

    @PostMapping("/delete/{id}")
    private BaseResponse<String> deleteSuratMasuk(@PathVariable Long id) {
        BaseResponse<String> response = new BaseResponse<>();
        try {
            SuratMasukModel suratMasuk = suratMasukService.getSuratMasuk(id);
            if (suratMasuk != null) {
                suratMasukService.deleteSuratMasuk(suratMasuk);
                response.setStatus(200);
                response.setMessage("success");
                response.setResult("Surat masuk berhasil dihapus");
            } else {
                throw new Exception("Surat masuk tidak ada di database");
            }
        } catch (Exception e) {
            response.setStatus(500);
            response.setMessage("failed");
            response.setResult(e.getMessage());
        }
        return response;

    }
}
