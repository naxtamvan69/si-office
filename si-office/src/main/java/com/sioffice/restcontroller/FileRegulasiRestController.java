package com.sioffice.restcontroller;

import com.sioffice.model.FileRegulasiEntity;
import com.sioffice.rest.FileResponse;
import com.sioffice.service.RegulasiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/file-regulasi")
public class FileRegulasiRestController {
    @Qualifier("regulasiServiceImpl")
    @Autowired
    private RegulasiService regulasiService;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFileRegulasi(@RequestParam("file")MultipartFile file) {
        try {
            FileRegulasiEntity fileRegulasi = new FileRegulasiEntity();
            regulasiService.saveFileRegulasi(file, fileRegulasi);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(String.format("File uploaded successfully: %s", file.getOriginalFilename()));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(String.format("Could not upload the file: %s!", file.getOriginalFilename()));
        }
    }

    @GetMapping("")
    public List<FileResponse> daftarFileRegulasi() {
        return regulasiService.getListFileRegulasi()
                .stream()
                .map(this::mapToFileResponse)
                .collect(Collectors.toList());
    }

    private FileResponse mapToFileResponse(FileRegulasiEntity fileRegulasi) {
        String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/file-regulasi/")
                .path(fileRegulasi.getId())
                .toUriString();

        FileResponse file = new FileResponse();
        file.setId(fileRegulasi.getId());
        file.setName(fileRegulasi.getName());
        file.setContentType(fileRegulasi.getContentType());
        file.setSize(fileRegulasi.getSize());
        file.setUrl(downloadURL);
        return file;
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getFileRegulasi(@PathVariable String id) {
        FileRegulasiEntity fileRegulasi = regulasiService.getFileRegulasi(id);

        if (fileRegulasi==null) {
            return ResponseEntity.notFound()
                    .build();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileRegulasi.getName() + "\"")
                .contentType(MediaType.valueOf(fileRegulasi.getContentType()))
                .body(fileRegulasi.getData());
    }
}
