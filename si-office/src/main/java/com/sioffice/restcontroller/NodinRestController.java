package com.sioffice.restcontroller;

import com.sioffice.model.NodinModel;
import com.sioffice.rest.BaseResponse;
import com.sioffice.service.NodinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/nodin")
public class NodinRestController {
    @Qualifier("nodinServiceImpl")
    @Autowired
    NodinService nodinService;

    @PostMapping("/delete/{nomor}")
    private BaseResponse<String> deleteNodin(@PathVariable Long nomor) {
        BaseResponse<String> response = new BaseResponse<>();
        try {
            NodinModel nodin = nodinService.getANotaDinas(nomor);
            if (nodin != null) {
                nodinService.deleteNotaDinas(nodin);
                response.setStatus(200);
                response.setMessage("success");
                response.setResult("Data berhasil dihapus");
            } else {
                throw new Exception("Data tidak ada di database");
            }
        } catch (Exception e) {
            response.setStatus(500);
            response.setMessage("failed");
            response.setResult(e.getMessage());
        }
        return response;
    }
}
