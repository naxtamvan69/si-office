package com.sioffice.restcontroller;

import com.sioffice.model.SuratKeluarModel;
import com.sioffice.model.SuratMasukModel;
import com.sioffice.rest.BaseResponse;
import com.sioffice.service.SuratKeluarService;
import com.sioffice.service.SuratMasukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/surat-keluar")
public class SuratKeluarRestController {
    @Qualifier("suratKeluarServiceImpl")
    @Autowired
    SuratKeluarService suratKeluarService;

    @PostMapping("/delete/{id}")
    private BaseResponse<String> deleteSuratKeluar(@PathVariable Long id) {
        BaseResponse<String> response = new BaseResponse<>();
        try {
            SuratKeluarModel suratKeluar = suratKeluarService.getSuratKeluar(id);
            if (suratKeluar != null) {
                suratKeluarService.deleteSuratKeluar(suratKeluar);
                response.setStatus(200);
                response.setMessage("success");
                response.setResult("Surat keluar berhasil dihapus");
            } else {
                throw new Exception("Surat keluar tidak ada di database");
            }
        } catch (Exception e) {
            response.setStatus(500);
            response.setMessage("failed");
            response.setResult(e.getMessage());
        }
        return response;

    }
}
