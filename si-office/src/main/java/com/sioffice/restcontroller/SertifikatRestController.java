package com.sioffice.restcontroller;

import com.sioffice.model.IjazahEntity;
import com.sioffice.model.SertifikatEntity;
import com.sioffice.rest.FileResponse;
import com.sioffice.service.KompetensiService;
import com.sioffice.service.RiwayatPendidikanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/file-sertifikat")
public class SertifikatRestController {
    @Qualifier("kompetensiServiceImpl")
    @Autowired
    private KompetensiService kompetensiService;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFileSertifikat(@RequestParam("file") MultipartFile file) {
        try {
            SertifikatEntity sertifikatFile = new SertifikatEntity();
            kompetensiService.saveSertifikat(file, sertifikatFile);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(String.format("File uploaded successfully: %s", file.getOriginalFilename()));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(String.format("Could not upload the file: %s!", file.getOriginalFilename()));
        }
    }

    @GetMapping("")
    public List<FileResponse> daftarFileSertifikat() {
        return kompetensiService.getListFileSertifikat()
                .stream()
                .map(this::mapToFileResponse)
                .collect(Collectors.toList());
    }

    private FileResponse mapToFileResponse(SertifikatEntity sertifikatFile) {
        String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/file-sertifikat/")
                .path(sertifikatFile.getId())
                .toUriString();

        FileResponse file = new FileResponse();
        file.setId(sertifikatFile.getId());
        file.setName(sertifikatFile.getName());
        file.setContentType(sertifikatFile.getContentType());
        file.setSize(sertifikatFile.getSize());
        file.setUrl(downloadURL);
        return file;
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getSertifikatFile(@PathVariable String id) {
        SertifikatEntity sertifikat = kompetensiService.getFileSertifikat(id);

        if (sertifikat==null) {
            return ResponseEntity.notFound()
                    .build();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + sertifikat.getName() + "\"")
                .contentType(MediaType.valueOf(sertifikat.getContentType()))
                .body(sertifikat.getData());
    }
}
