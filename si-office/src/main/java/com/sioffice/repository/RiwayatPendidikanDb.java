package com.sioffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sioffice.model.RiwayatPendidikan;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RiwayatPendidikanDb extends JpaRepository<RiwayatPendidikan, Integer>{

}
