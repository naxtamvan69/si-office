package com.sioffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sioffice.model.KompetensiModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KompetensiDb extends JpaRepository<KompetensiModel, Integer> {
}
