package com.sioffice.repository;

import com.sioffice.model.SuratMasukModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SuratMasukDb extends JpaRepository<SuratMasukModel, Long> {
    Optional<SuratMasukModel> findByIdSuratMasuk(Long idSuratMasuk);

    @Query("select i from SuratMasukModel i where lower(i.perihal) like %:perihal%")
    List<SuratMasukModel> findByFilterPerihal(@Param("perihal") String perihal);

    @Query("select i from SuratMasukModel i where i.tanggal = :tanggal")
    List<SuratMasukModel> findByFilterDate(@Param("tanggal") LocalDate tanggal);

    @Query(value = "select i from SuratMasukModel i where lower(i.perihal) like %:perihal%",
    countQuery = "select count(i) from SuratMasukModel i where lower(i.perihal) like %:perihal%")
    Page<SuratMasukModel> findByFilterPerihalAndPageAndSize(String perihal, Pageable pageable);

    @Query(value = "select i from SuratMasukModel i where i.tanggal = :tanggal",
    countQuery = "select count(i) from SuratMasukModel i where i.tanggal = :tanggal")
    Page<SuratMasukModel> findByFilterTanggalAndPageAndSize(LocalDate tanggal, Pageable pageable);
}
