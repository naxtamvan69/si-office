package com.sioffice.repository;

import com.sioffice.model.VendorModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VendorDb extends JpaRepository<VendorModel, Integer> {
    //Custom query
    @Query(value = "select * from vendor v where lower(v.nama_vendor) like %:keyword% or lower(v.jenis_supplier) like %:keyword%", nativeQuery = true)
    List<VendorModel> findByKeyword(@Param("keyword") String keyword);
}
