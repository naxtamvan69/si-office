package com.sioffice.repository;

import com.sioffice.model.LogoRekananEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogoRekananEntityDb extends JpaRepository<LogoRekananEntity, String>{
    
}
