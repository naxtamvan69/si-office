package com.sioffice.repository;

import com.sioffice.model.PenggunaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KaryawanDb extends JpaRepository<PenggunaModel, Integer> {
    
}
