package com.sioffice.repository;

import com.sioffice.model.FileSuratKeluarEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileSuratKeluarEntityDb extends JpaRepository<FileSuratKeluarEntity, String> {
}
