package com.sioffice.repository;

import com.sioffice.model.SertifikatEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SertifikatDb extends JpaRepository<SertifikatEntity, String> {
}
