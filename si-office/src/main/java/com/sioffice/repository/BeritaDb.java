package com.sioffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sioffice.model.BeritaModel;

import java.util.Optional;

public interface BeritaDb extends JpaRepository<BeritaModel, Integer>{
    Optional<BeritaModel> findByNoBerita(Long noBerita);
}