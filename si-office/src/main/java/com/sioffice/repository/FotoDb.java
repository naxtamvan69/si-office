package com.sioffice.repository;

import com.sioffice.model.FotoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FotoDb extends JpaRepository<FotoEntity, String> {
}
