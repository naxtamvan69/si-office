package com.sioffice.repository;

import com.sioffice.model.FaqModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FaqDb extends JpaRepository<FaqModel, Long> {
    Optional<FaqModel> findByIdFaq(Long idFaq);
}
