package com.sioffice.repository;

import com.sioffice.model.SuratKeluarModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SuratKeluarDb extends JpaRepository<SuratKeluarModel, Long> {
    Optional<SuratKeluarModel> findByIdSuratKeluar(Long idSuratKeluar);

    @Query("select i from SuratKeluarModel i where lower(i.perihal) like %:perihal%")
    List<SuratKeluarModel> findAllByPerihal(@Param("perihal") String perihal);

    @Query("select i from SuratKeluarModel i where i.tanggal = :tanggal")
    List<SuratKeluarModel> findAllByDate(@Param("tanggal") LocalDate tanggal);

    @Query(value = "select i from SuratKeluarModel i where lower(i.perihal) like %:perihal%",
    countQuery = "select count(i) from SuratKeluarModel i where lower(i.perihal) like %:perihal%")
    Page<SuratKeluarModel> findAllByPerihalAndPageAndSize(String perihal, Pageable pageable);

    @Query(value = "select i from SuratKeluarModel i where i.tanggal = :tanggal",
    countQuery = "select count(i) from SuratKeluarModel i where i.tanggal = :tanggal")
    Page<SuratKeluarModel> findAllByDateAndPageAndSize(LocalDate tanggal, Pageable pageable);
}
