package com.sioffice.repository;

import org.springframework.data.jpa.repository.Query;
import com.sioffice.model.PenggunaModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordRepository extends CrudRepository<PenggunaModel, Integer> {

    @Query("SELECT c FROM PenggunaModel c WHERE c.email = ?1")
    public PenggunaModel findByEmail(String email);

    public PenggunaModel findByResetPasswordToken(String token);
}
