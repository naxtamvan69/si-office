package com.sioffice.repository;

import com.sioffice.model.FileSuratMasukEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileSuratMasukEntityDb extends JpaRepository<FileSuratMasukEntity, String> {
}
