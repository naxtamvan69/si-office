package com.sioffice.repository;

import com.sioffice.model.NodinModel;
import com.sioffice.model.PenggunaModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface NodinDb extends JpaRepository<NodinModel, Long>{
    List<NodinModel> findAllByPenggunaOrderByNomorDesc(PenggunaModel pengguna);
    List<NodinModel> findAllByStatusOrderByNomorDesc(int status);
    List<NodinModel> findAllByStatusOrderByTanggalPengajuanAsc(int status);

    @Query("select  i from NodinModel i where (i.status=4 or i.status=5) and lower(i.perihal) like %:perihal%")
    List<NodinModel> findAllByPerihal(@Param("perihal") String perihal);

    @Query(value = "select i from NodinModel i where (i.status=4 or i.status=5) and i.tanggalPengajuan = :tanggal")
    List<NodinModel> findAllByTanggalPengajuan(@Param("tanggal") LocalDate tanggal);

    @Query(value = "select i from NodinModel i where (i.status=4 or i.status=5) and i.tanggalPersetujuanManajer = :tanggal")
    List<NodinModel> findAllByTanggalPersetujuanManajer(@Param("tanggal")LocalDate tanggal);

    @Query(value = "select i from NodinModel  i where i.status=4 or i.status=5")
    List<NodinModel> findAllDisetujiAndTidakDisetuji();

    @Query(value = "select i from NodinModel  i where i.status=4 or i.status=5",
    countQuery = "select count(i) from NodinModel  i where i.status=4 or i.status=5")
    Page<NodinModel> findAllDisetujiAndTidakDisetujiByPageAndSize(Pageable pageable);

    @Query(value = "select  i from NodinModel i where (i.status=4 or i.status=5) and lower(i.perihal) like %:perihal%",
            countQuery = "select count(i) from NodinModel i where (i.status=4 or i.status=5) and lower(i.perihal) like %:perihal%")
    Page<NodinModel> findAllByPerihalAndPageAndSize(@Param("perihal") String perihal, Pageable pageable);

    @Query(value = "select i from NodinModel i where (i.status=4 or i.status=5) and i.tanggalPengajuan = :tanggal",
            countQuery = "select  count(i) from NodinModel i where (i.status=4 or i.status=5) and i.tanggalPengajuan = :tanggal")
    Page<NodinModel> findAllByTanggalPengajuanAndPageAndSize(@Param("tanggal")LocalDate tanggal, Pageable pageable);

    @Query(value = "select i from NodinModel i where (i.status=4 or i.status=5) and i.tanggalPersetujuanManajer = :tanggal",
            countQuery = "select  count(i) from NodinModel i where (i.status=4 or i.status=5) and i.tanggalPersetujuanManajer = :tanggal")
    Page<NodinModel> findAllByTanggalPersetujuanManajerAndPageAndSize(@Param("tanggal")LocalDate tanggal, Pageable pageable);

    @Query(value = "select  i from NodinModel i where i.status=4",
            countQuery = "select  count(i) from NodinModel i where i.status=4")
    Page<NodinModel> findAllByDisetujuiDirekturAndPageAndSize(Pageable pageable);

    @Query(value = "select  i from NodinModel i where i.status=5",
            countQuery = "select  count(i) from NodinModel i where i.status=5")
    Page<NodinModel> findAllByDitolakDirekturAndPageAndSize(Pageable pageable);
}