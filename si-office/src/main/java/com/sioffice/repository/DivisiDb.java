package com.sioffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sioffice.model.DivisiModel;

public interface DivisiDb extends JpaRepository<DivisiModel, Integer>{
    
}
