package com.sioffice.repository;

import java.util.Optional;

import com.sioffice.model.PenggunaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PenggunaDb extends JpaRepository<PenggunaModel,Integer> {
    PenggunaModel findByUsername(String username);
    PenggunaModel findByEmail(String email);
}
