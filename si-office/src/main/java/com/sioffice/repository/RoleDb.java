package com.sioffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sioffice.model.RoleModel;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDb extends JpaRepository<RoleModel, Integer>{

}
