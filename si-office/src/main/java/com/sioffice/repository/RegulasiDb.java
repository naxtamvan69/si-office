package com.sioffice.repository;

import com.sioffice.model.RegulasiModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface RegulasiDb extends JpaRepository<RegulasiModel, Long> {
    Optional<RegulasiModel> findByIdRegulasi(Long idRegulasi);

    @Query("select i from RegulasiModel i where lower(i.perihal) like %:perihal%")
    List<RegulasiModel> findByFilterPerihal(@Param("perihal") String perihal);

    @Query("select i from RegulasiModel i where i.tanggal = :tanggal")
    List<RegulasiModel> findByFilterDate(@Param("tanggal") LocalDate tanggal);

    @Query(value = "select i from RegulasiModel i where lower(i.perihal) like %:perihal%",
    countQuery = "select count(i) from RegulasiModel i where lower(i.perihal) like %:perihal%")
    Page<RegulasiModel> findByFilterPerihalAndPageAndSize(String perihal, Pageable pageable);

    @Query(value = "select i from RegulasiModel i where i.tanggal = :tanggal",
    countQuery = "select count(i) from RegulasiModel i where i.tanggal = :tanggal")
    Page<RegulasiModel> findByFilterDateAndPageAndSize(LocalDate tanggal, Pageable pageable);
}
