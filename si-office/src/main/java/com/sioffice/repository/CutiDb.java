package com.sioffice.repository;

import com.sioffice.model.FaqModel;
import com.sioffice.model.PenggunaModel;
import org.springframework.data.jpa.repository.JpaRepository;

import com.sioffice.model.CutiModel;

import java.util.List;
import java.util.Optional;

public interface CutiDb extends JpaRepository<CutiModel, Integer>{
    Optional<CutiModel> findByNoCuti(Long noCuti);
    List<CutiModel> findAllByStatus(String status);
}
