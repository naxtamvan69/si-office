package com.sioffice.repository;

import com.sioffice.model.ProdukVendorModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdukVendorDb extends JpaRepository<ProdukVendorModel, Integer> {
}
