package com.sioffice.repository;

import com.sioffice.model.IjazahEntity;
import com.sioffice.model.RegulasiModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IjazahDb extends JpaRepository<IjazahEntity, String> {
}
