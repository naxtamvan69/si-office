package com.sioffice.repository;

import com.sioffice.model.RekananModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RekananDb extends JpaRepository<RekananModel, Long> {
    
}
