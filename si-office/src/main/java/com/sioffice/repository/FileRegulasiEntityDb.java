package com.sioffice.repository;

import com.sioffice.model.FileRegulasiEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRegulasiEntityDb extends JpaRepository<FileRegulasiEntity, String> {
}
