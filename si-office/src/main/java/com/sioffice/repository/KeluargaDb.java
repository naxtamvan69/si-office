package com.sioffice.repository;

import com.sioffice.model.KeluargaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeluargaDb extends JpaRepository<KeluargaModel, Integer> {
}
