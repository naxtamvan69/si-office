package com.sioffice.rest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UbahUrutanFaqDTO implements Serializable {
    private Long id;
    private int urutanSebelum;
    private int urutanUpdate;
}
