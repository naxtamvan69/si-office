package com.sioffice.rest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FileResponse implements Serializable{
    private String id;
    private String name;
    private Long size;
    private String url;
    private String contentType;
}
