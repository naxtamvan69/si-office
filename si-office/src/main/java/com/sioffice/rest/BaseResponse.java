package com.sioffice.rest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class BaseResponse<T> implements Serializable {
    private int status;
    private String message;
    private T result;

}
