package com.sioffice.rest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ListUbahUrutanFaqDTO implements Serializable {
    private List<UbahUrutanFaqDTO> data;
}
