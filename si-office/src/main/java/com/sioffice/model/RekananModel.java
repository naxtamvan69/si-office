package com.sioffice.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "rekanan")
public class RekananModel implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRekanan;

    @NotNull
    @Size(max = 100)
    @Column(name = "nama_rekanan", nullable = false, unique = true)
    private String nama_rekanan;

    @Size(max=500)
    @Column(name="alamat", nullable = false)
    private String alamat;

    @Size(max=30)
    @Column(name="telp", nullable = false)
    private String telp;

    @NotNull
    @Column(name = "profil_rekanan", nullable = false, columnDefinition="TEXT")
    private String profil_rekanan;

    @OneToOne(mappedBy = "rekanan", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private LogoRekananEntity logorekanan;
}
