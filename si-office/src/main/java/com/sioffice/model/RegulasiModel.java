package com.sioffice.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "regulasi")
public class RegulasiModel implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRegulasi;

    @NotNull
    @Column(name = "nomor", nullable = false, unique = true)
    private String nomor;

    @NotNull
    @Size(max = 200)
    @Column(name = "perihal", nullable = false)
    private String perihal;

    @NotNull
    @Column(name = "tanggal", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggal;

    @OneToOne(mappedBy = "regulasi", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private FileRegulasiEntity fileRegulasi;
}
