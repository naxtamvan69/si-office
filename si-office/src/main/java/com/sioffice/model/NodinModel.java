package com.sioffice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Setter @Getter
@Entity
@Table(name = "nota_dinas")
public class NodinModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long nomor;

    private LocalDate tanggalPengajuan;

    private LocalDate tanggalPersetujuanManajer;

    @NotNull
    @Size(max = 200)
    private String perihal;

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String pembuka;

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String inti;

    @NotNull
    private int status;
    /*
    Status surat antara lain,
    0: draf (default)
    1: diajukan ke manajer
    2: disetujui manajer/diajukan ke direktur
    3: ditolak manajer
    4: diterima direktur/diteruskan ke arsip
    5: ditunda direktur
     */

    @Column(columnDefinition = "TEXT")
    private String alasanPenolakan;

    @Column(columnDefinition = "TEXT")
    private String disposisi;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "pengguna", referencedColumnName = "username", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PenggunaModel pengguna;
}
