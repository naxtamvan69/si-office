package com.sioffice.model;

import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "cuti")
public class CutiModel implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long noCuti;

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggal_mulai;

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggal_akhir;

    @Nullable
    private long durasi;

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date tanggal_pengajuan = new Date();

    @NotNull
    @Column(nullable = false)
    private String jenis;

    @NotNull
    @Column(nullable = false)
    private String status = "Menunggu Persetujuan Supervisi";

    @NotNull
    @Lob
    @Column(columnDefinition="TEXT")
    private String alasan_cuti;

//    @NotNull
//    @Column(nullable = false)
//    private String lokasi;

    @Nullable
    @Lob
    @Column(columnDefinition="TEXT")
    private String alasan_penolakan;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "id_pengguna", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PenggunaModel pengguna;

    @Nullable
    private String penolak;
}
