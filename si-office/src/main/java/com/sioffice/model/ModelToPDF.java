package com.sioffice.model;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;


public class ModelToPDF {
    public ModelToPDF() {

    }

    public void exportNodin(HttpServletResponse response, NodinModel nodin, PenggunaModel pengguna) throws IOException {
        PdfWriter writer = new PdfWriter(response.getOutputStream());
        PdfDocument pdf = new PdfDocument(writer);
        Document document = new Document(pdf);
        document.setMargins(50, 50, 50, 50);

        Style title = new Style();
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        title.setFont(font).setFontSize(14).setUnderline();
        Style normal = new Style();
        font = PdfFontFactory.createFont(FontConstants.COURIER);
        normal.setFont(font).setFontSize(11);
        Style helvetica = new Style();
        font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        helvetica.setFont(font).setFontSize(11);

        Paragraph kepalaSurat = new Paragraph("ORCHID GRIYA NUTRISI\n" + "PT. RUMAH SAKIT PELABUHAN").addStyle(helvetica);
        kepalaSurat.setMarginBottom(10);
        document.add(kepalaSurat);

        Paragraph judulSurat = new Paragraph("NOTA DINAS").addStyle(title);
        judulSurat.setTextAlignment(TextAlignment.CENTER);
        document.add(judulSurat);

        Paragraph nomorSurat = new Paragraph("Nomor: OPS/" + nodin.getNomor() + "/OGN.Jkt-2022").addStyle(normal);
        nomorSurat.setTextAlignment(TextAlignment.CENTER);
        nomorSurat.setMarginBottom(20);
        document.add(nomorSurat);

        float[] columnWidths = {1, 5};
        Table tabelPembukaSurat = new Table(UnitValue.createPercentArray(columnWidths));

        Cell a1 = new Cell();
        a1.add(new Paragraph("Kepada   :").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell b1 = new Cell();
        b1.add(new Paragraph("Direktur Orchid Griya Nutrisi").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell a2 = new Cell();
        a2.add(new Paragraph("Dari     :").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell b2 = new Cell();
        b2.add(new Paragraph("Manajer Unit").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell a3 = new Cell();
        a3.add(new Paragraph("Perihal  :").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell b3 = new Cell();
        b3.add(new Paragraph(nodin.getPerihal()).addStyle(normal)).setBorder(Border.NO_BORDER);
        tabelPembukaSurat.addCell(a1);
        tabelPembukaSurat.addCell(b1);
        tabelPembukaSurat.addCell(a2);
        tabelPembukaSurat.addCell(b2);
        tabelPembukaSurat.addCell(a3);
        tabelPembukaSurat.addCell(b3);
        tabelPembukaSurat.setMarginBottom(15);
        document.add(tabelPembukaSurat);

        columnWidths = new float[]{2f, 40f};
        Table tabelIsiSurat = new Table(columnWidths);
        tabelIsiSurat.setWidthPercent(85);
        tabelIsiSurat.setHorizontalAlignment(HorizontalAlignment.RIGHT);

        Cell c1 = new Cell();
        c1.add(new Paragraph("1.").addStyle(normal)).setBorder(Border.NO_BORDER).setPaddingBottom(20);
        Cell d1 = new Cell();
        d1.add(new Paragraph("Menindaklanjuti: \n" + nodin.getPembuka()).addStyle(normal).setTextAlignment(TextAlignment.JUSTIFIED)).setBorder(Border.NO_BORDER);
        Cell c2 = new Cell();
        c2.add(new Paragraph("2.").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell d2 = new Cell().setTextAlignment(TextAlignment.JUSTIFIED);
        d2.add(new Paragraph("Dengan surat ini kami hendak menyampaikan: \n " + nodin.getInti()).addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell c3 = new Cell();
        c3.add(new Paragraph("3.").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell d3 = new Cell();
        d3.add(new Paragraph("Sekian dari kami, terima kasih atas perhatiannya").addStyle(normal)).setBorder(Border.NO_BORDER);
        tabelIsiSurat.addCell(c1);
        tabelIsiSurat.addCell(d1);
        tabelIsiSurat.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(" ")));
        tabelIsiSurat.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(" ")));
        tabelIsiSurat.addCell(c2);
        tabelIsiSurat.addCell(d2);
        tabelIsiSurat.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(" ")));
        tabelIsiSurat.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(" ")));
        tabelIsiSurat.addCell(c3);
        tabelIsiSurat.addCell(d3);
        tabelIsiSurat.setMarginBottom(15);
        document.add(tabelIsiSurat);


        if (nodin.getStatus() == 2 || nodin.getStatus() == 4 || nodin.getStatus() == 5 ) {
            document.add(new Paragraph("\n"));
            Table tabelPersetujuanDanDisposisi = new Table(2);
            tabelPersetujuanDanDisposisi.setWidthPercent(100);
            Cell e1 = new Cell().setBorder(Border.NO_BORDER);
            Cell f1 = new Cell().setBorder(Border.NO_BORDER);

            String tanggal = nodin.getTanggalPersetujuanManajer().format(DateTimeFormatter.ofPattern("dd MMMM yyyy"));
            Paragraph headerTtd = new Paragraph("Jakarta, " + tanggal + '\n' +
                    "Orchid Griya Nutrisi PT. RSP" + '\n' +
                    "Manajer Unit"
            ).addStyle(helvetica);
            headerTtd.setTextAlignment(TextAlignment.RIGHT);
            f1.add(headerTtd);

            ImageData data = ImageDataFactory.create(pengguna.getTtdPengguna().getData());
            Image img = new Image(data);
            img.scaleAbsolute(100f, 100f);
            img.setHorizontalAlignment(HorizontalAlignment.RIGHT);
            f1.add(img);

            f1.add(new Paragraph("Ahad Fajar Samsudin").setTextAlignment(TextAlignment.RIGHT).addStyle(helvetica));

            tabelPersetujuanDanDisposisi.addCell(e1);
            tabelPersetujuanDanDisposisi.addCell(f1);

            if (nodin.getStatus() == 4 || nodin.getStatus() == 5) {
                String status;
                if (nodin.getStatus() == 4) {
                    status = "DISETUJUI";
                } else {
                    status = "DITUNDA";
                }
                Paragraph disposisi = new Paragraph("Disposisi : ").addStyle(normal);
                disposisi.add(new Text(status).setBold());
                e1.add(disposisi).setTextAlignment(TextAlignment.LEFT);
                e1.add(new Paragraph("Catatan   : \n" + nodin.getDisposisi()).setTextAlignment(TextAlignment.LEFT).addStyle(normal));
            }
            document.add(tabelPersetujuanDanDisposisi);
        }
        document.close();
    }
}