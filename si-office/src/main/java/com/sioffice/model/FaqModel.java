package com.sioffice.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "faq")
public class FaqModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idFaq;

    @NotNull
    @Column(name = "pertanyaan", nullable = false, columnDefinition="TEXT")
    private String pertanyaan;

    @NotNull
    @Column(name = "jawaban", nullable = false, columnDefinition="TEXT")
    private String jawaban;

    @NotNull
    @Column(name = "no_urutan", nullable = false)
    private Integer noUrutan;
}
