package com.sioffice.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "vendor")
public class VendorModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Size(max = 100)
    @Column(name = "nama_vendor", nullable = false, unique = true)
    private String nama_vendor;

    @Size(max=500)
    @Column(name="alamat")
    private String alamat;

    @Size(max=30)
    @Column(name="no_hp")
    private String no_hp;

    @Size(max = 100)
    @Column(name = "email")
    private String email;

    @Size(max = 100)
    @Column(name = "jenis_supplier")
    private String jenis_supplier;

    @NotNull
    @Column(name="jumlah_produk", nullable = false)
    private Integer jumlah_produk = 0;

    @OneToMany(mappedBy = "vendor", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProdukVendorModel> listProduk;
}
