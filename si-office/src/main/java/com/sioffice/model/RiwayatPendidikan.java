package com.sioffice.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "riwayat_pendidikan")
public class RiwayatPendidikan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer no_riwayat;

    @NotNull
    @Size(max=100)
    @Column(name = "jenjang", nullable = false)
    private String jenjang;

    @Size(max=4)
    @Column(name = "tahun_lulus", nullable = false)
    private String tahun_lulus;

    @Size(max=100)
    @Column(name = "nama", nullable = false)
    private String nama;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "pengguna", referencedColumnName = "username", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PenggunaModel pengguna;

    @OneToOne(mappedBy = "riwayatPendidikan", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private IjazahEntity ijazahFile;

}
