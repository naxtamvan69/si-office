package com.sioffice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "kompetensi")
public class KompetensiModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer no_kompetensi;

    @NotNull
    @Size(max=30)
    @Column(name = "topik", nullable = false)
    private String topik;

    @NotNull
    @Size(max=4)
    @Column(name = "tahun", nullable = false)
    private String tahun;

    @NotNull
    @Size(max=50)
    @Column(name = "lokasi", nullable = false)
    private String lokasi;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "pengguna", referencedColumnName = "username", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PenggunaModel pengguna;

    @OneToOne(mappedBy = "kompetensi", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private SertifikatEntity sertifikatFile;
}
