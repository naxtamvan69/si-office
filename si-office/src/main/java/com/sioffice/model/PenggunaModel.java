package com.sioffice.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "pengguna")
public class PenggunaModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(max = 10)
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @NotNull
    @Size(max = 50)
    @Column(name = "nama_depan", nullable = false)
    private String nama_depan;

    @NotNull
    @Size(max = 50)
    @Column(name = "nama_belakang", nullable = false)
    private String nama_belakang;

    @NotNull
    @Size(max = 50)
    @Column(name = "status_pegawai_SPK", nullable = false)
    private String status_pegawai_SPK;

    @NotNull
    @Size(max = 50)
    @Column(name = "tempat_lahir", nullable = false)
    private String tempat_lahir;

    @NotNull
    @Column(name = "tanggal_lahir", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggal_lahir;

    @NotNull
    @Column(name = "umur", nullable = false)
    private Integer umur;

    @NotNull
    @Column(name = "jml_tanggungan", nullable = false)
    private Integer jml_tanggungan;

    @NotNull
    @Size(max = 100)
    @Column(name = "jabatan", nullable = false)
    private String jabatan;

    @Column(name = "kelas_jabatan")
    private Integer kelas_jabatan;

    @NotNull
    @Column(name = "tanggal_masuk", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggal_masuk;

    @Column(name = "tgl_awal_percobaan")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tgl_awal_percobaan;

    @Column(name = "tgl_akhir_percobaan")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tgl_akhir_percobaan;

    @Column(name = "tgl_awal_kontrak")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tgl_awal_kontrak;

    @Column(name = "tgl_akhir_kontrak")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tgl_akhir_kontrak;

    @Size(max = 50)
    @Column(name = "status_pernikahan", nullable = false)
    private String status_pernikahan;

    @NotNull
    @Size(max=500)
    @Column(name="alamat", nullable = false)
    private String alamat;

    @NotNull
    @Size(max=30)
    @Column(name="no_hp", nullable = false)
    private String no_hp;

    @NotNull
    @Column(name = "jenis_kelamin", nullable = false)
    private Integer jenis_kelamin;

    @NotNull
    @Size(max=16)
    @Column(name="nik", nullable = false)
    private String nik;

    @NotNull
    @Size(max=16)
    @Column(name="no_kk", nullable = false)
    private String no_kk;

    @NotNull
    @Size(max=11)
    @Column(name="bpjs_ketenagakerjaan", nullable = false)
    private String bpjs_ketenagakerjaan;

    @NotNull
    @Size(max=13)
    @Column(name="bpjs_kesehatan", nullable = false)
    private String bpjs_kesehatan;

    @NotNull
    @Size(max = 100)
    @Column(name = "email", nullable = false)
    private String email;

    @Size(max = 100)
    @Column(name = "ibu_kandung")
    private String ibu_kandung;

    @NotNull
    @Column(name = "password", nullable = false)
    private String password;

    @NotNull
    @Column(name="sisa_cuti", nullable = false)
    private Integer sisa_cuti = 12;

    @Column(name = "reset_password_token")
    private String resetPasswordToken;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private RoleModel role;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "divisi", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private DivisiModel divisi;

    @OneToMany(mappedBy = "pengguna", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<KompetensiModel> listKompetensi;

    @OneToMany(mappedBy = "pengguna", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RiwayatPendidikan> listPendidikan;

    @OneToOne(mappedBy = "pengguna", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private FileTtdEntity TtdPengguna;

    @OneToOne(mappedBy = "pengguna", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private FotoEntity FotoPengguna;

    @OneToMany(mappedBy = "pengguna", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<KeluargaModel> listKeluarga;

    @OneToMany(mappedBy = "pengguna", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CutiModel> listCuti;

    @OneToMany(mappedBy = "pengguna", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NodinModel> listNotaDinas;

}
