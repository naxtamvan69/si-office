package com.sioffice.model;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

import java.util.List;

public class CVModel {
    public CVModel(){

    }

    public void downloadCV(HttpServletResponse response, PenggunaModel pengguna) throws IOException {
        PdfWriter writer = new PdfWriter(response.getOutputStream());
        PdfDocument pdf = new PdfDocument(writer);
        Document document = new Document(pdf);
        document.setMargins(50, 50, 50, 50);
        
        Style title = new Style();
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        title.setFont(font).setFontSize(18).setUnderline();
        Style normal = new Style();
        font = PdfFontFactory.createFont(FontConstants.COURIER);
        normal.setFont(font).setFontSize(12);
        Style helvetica = new Style();
        font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        helvetica.setFont(font).setFontSize(12);

        if (pengguna.getFotoPengguna() != null){
            ImageData foto = ImageDataFactory.create(pengguna.getFotoPengguna().getData());
            Image img = new Image(foto);
            img.scaleAbsolute(100f, 100f);
            img.setHorizontalAlignment(HorizontalAlignment.CENTER);
            document.add(img);
            document.add(new Paragraph(" ").addStyle(normal));
        }

        Paragraph judul = new Paragraph(pengguna.getNama_depan() + " " + pengguna.getNama_belakang()).addStyle(title);
        judul.setTextAlignment(TextAlignment.CENTER);
        document.add(judul);
        document.add(new Paragraph(" ").addStyle(normal));

        float[] columnWidths = {5, 10};
        Table tabelCVAtas = new Table(UnitValue.createPercentArray(columnWidths));
        tabelCVAtas.setHorizontalAlignment(HorizontalAlignment.CENTER);
        

        Cell kiriAlamat = new Cell();
        kiriAlamat.add(new Paragraph("Address   :").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell alamat = new Cell();
        alamat.add(new Paragraph(pengguna.getAlamat()).addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell kiriEmail = new Cell();
        kiriEmail.add(new Paragraph("Email     :").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell email = new Cell();
        email.add(new Paragraph(pengguna.getEmail()).addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell kiriTelp = new Cell();
        kiriTelp.add(new Paragraph("Mobile    :").addStyle(normal)).setBorder(Border.NO_BORDER);
        Cell telp = new Cell();
        telp.add(new Paragraph(pengguna.getNo_hp()).addStyle(normal)).setBorder(Border.NO_BORDER);

        tabelCVAtas.addCell(kiriAlamat);
        tabelCVAtas.addCell(alamat);
        tabelCVAtas.addCell(kiriEmail);
        tabelCVAtas.addCell(email);
        tabelCVAtas.addCell(kiriTelp);
        tabelCVAtas.addCell(telp);
        tabelCVAtas.setMarginBottom(15);
        document.add(tabelCVAtas);

        if (pengguna.getListKompetensi() != null){
            columnWidths = new float[]{1, 20};
            Table tabelKompetensi = new Table(UnitValue.createPercentArray(columnWidths));
            tabelKompetensi.setHorizontalAlignment(HorizontalAlignment.CENTER);
            tabelKompetensi.setTextAlignment(TextAlignment.CENTER);

            Cell kananKompetensi = new Cell();
            kananKompetensi.add(new Paragraph("Competence").addStyle(helvetica)).setBorder(Border.NO_BORDER);
            tabelKompetensi.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(" ")));
            tabelKompetensi.addCell(kananKompetensi);

            List<KompetensiModel> listKompetensi = pengguna.getListKompetensi();
            for (int i = 0; i < listKompetensi.size(); i++){
                String topik = listKompetensi.get(i).getTopik();
                String lokasi = listKompetensi.get(i).getLokasi();
                String tahun = listKompetensi.get(i).getTahun();

                Cell kompetensi = new Cell();
                kompetensi.add(new Paragraph(topik + '\n' + lokasi + '\n' + tahun + '\n').addStyle(normal)).setBorder(Border.NO_BORDER);
                tabelKompetensi.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(" ")));
                tabelKompetensi.addCell(kompetensi);
            }
            tabelKompetensi.setMarginBottom(15);
            document.add(tabelKompetensi);
        }
        
        if (pengguna.getListKeluarga() != null){
            columnWidths = new float[]{1, 9};
            Table tabelPendidikan = new Table(UnitValue.createPercentArray(columnWidths));
            tabelPendidikan.setHorizontalAlignment(HorizontalAlignment.CENTER);
            tabelPendidikan.setTextAlignment(TextAlignment.CENTER);

            Cell kananPendidikan = new Cell();
            kananPendidikan.add(new Paragraph("Educational History").addStyle(helvetica)).setBorder(Border.NO_BORDER);
            tabelPendidikan.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(" ")));
            tabelPendidikan.addCell(kananPendidikan);

            List<RiwayatPendidikan> listPendidikan = pengguna.getListPendidikan();
            for (int i = 0; i < listPendidikan.size(); i++){
                String namasekolah = listPendidikan.get(i).getNama();
                String tahun = listPendidikan.get(i).getTahun_lulus();

                Cell pendidikan = new Cell();
                pendidikan.add(new Paragraph(namasekolah + '\n' + "Tahun Lulus " + tahun + '\n').addStyle(normal)).setBorder(Border.NO_BORDER);
                tabelPendidikan.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(" ")));
                tabelPendidikan.addCell(pendidikan);
            }
            tabelPendidikan.setMarginBottom(15);
            document.add(tabelPendidikan);
        }
        
        document.close();


    }
    
}
