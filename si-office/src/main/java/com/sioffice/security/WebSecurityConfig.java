package com.sioffice.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/api/**").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/forgot_password").permitAll()
                .antMatchers("/reset_password").permitAll()
                .antMatchers("/pengguna/profil").permitAll()
                .antMatchers("/pengguna/kompetensi").permitAll()
                .antMatchers("/pengguna/keluarga").permitAll()
                .antMatchers("/pengguna/riwayatPendidikan").permitAll()
                .antMatchers("/pengguna/ttd").permitAll()
                .antMatchers("/vendor").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/vendor/tambah").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/vendor/update/**").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/vendor/delete/**").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/vendor/view/**").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/vendor/tambahProduk/**").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/vendor/updateProduk/**").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/vendor/deleteProduk/**").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/vendor/search/**").hasAnyAuthority("Purchasing", "Admin")
                .antMatchers("/datakaryawan/tambah").hasAnyAuthority("SDM")
                .antMatchers("/datakaryawan/update/**").hasAnyAuthority("SDM")
                .antMatchers("/datakaryawan/delete/**").hasAnyAuthority("SDM")
                .antMatchers("/regulasi/ungah").hasAuthority("SDM")
                .antMatchers("/faq/tambah").hasAuthority("SDM")
                .antMatchers("/faq/edit/**").hasAuthority("SDM")
                .antMatchers("/faq/hapus/**").hasAuthority("SDM")
                .antMatchers("/faq/ubah-urutan").hasAuthority("SDM")
                .antMatchers("/berita/buat").hasAuthority("SDM")
                .antMatchers("/berita/ubah").hasAuthority("SDM")
                .antMatchers("/cuti/daftar-pengajuan").hasAnyAuthority("Direktur", "Manager", "Supervisor", "SDM","Admin")
                .antMatchers("/cuti/pengajuan/**").hasAnyAuthority("Direktur", "Manager", "Supervisor")
                .antMatchers("/cuti/kelola/**").hasAnyAuthority("Direktur", "Manager", "Supervisor")
                .antMatchers("/arsip/nota-dinas/").hasAuthority("SDM")
                .antMatchers("/notadinas/unduh/**").hasAnyAuthority("Operasional", "Marketing", "Manager", "Direktur", "SDM")
                .antMatchers("/notadinas").hasAnyAuthority("Operasional", "Marketing", "Manager", "Direktur")
                .antMatchers("/notadinas/detail/**").hasAnyAuthority("Operasional", "Marketing", "Manager", "Direktur")
                .antMatchers("/notadinas/add").hasAnyAuthority("Operasional", "Marketing")
                .antMatchers("/notadinas/edit").hasAnyAuthority("Operasional", "Marketing")
                .antMatchers("/notadinas/hapus").hasAnyAuthority("Operasional", "Marketing")
                .antMatchers("/notadinas/ajukan").hasAnyAuthority("Operasional", "Marketing")
                .antMatchers("/notadinas/setujui/**").hasAuthority("Manager")
                .antMatchers("/notadinas/tolak/**").hasAuthority("Manager")
                .antMatchers("/notadinas/disposisi/**").hasAuthority("Direktur")
                .antMatchers("/rekanan/tambah").hasAnyAuthority("Marketing")
                .antMatchers("/rekanan/update/**").hasAnyAuthority("Marketing")
                .antMatchers("/rekanan/delete/**").hasAnyAuthority("Marketing")
                .antMatchers("/surat-masuk/**").hasAuthority("SDM")
                .antMatchers("/surat-keluar/**").hasAuthority("SDM")
                .antMatchers("/regulasi/edit/**").hasAuthority("SDM")
                .antMatchers("/regulasi/delete/**").hasAuthority("SDM")
                .antMatchers("/surat-keluar/**").hasAuthority("SDM")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll()
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout").permitAll();
    }

    @Bean
    public BCryptPasswordEncoder encoder() {return new BCryptPasswordEncoder();}

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .passwordEncoder(encoder())
//                .withUser("301458789").password(encoder().encode("password")).roles("USER");
//    }

    @Override
    public void configure(WebSecurity web) throws Exception{
        web.ignoring().antMatchers("/api/**");
    }

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
    }
}
