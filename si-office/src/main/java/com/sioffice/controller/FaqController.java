package com.sioffice.controller;

import com.sioffice.model.FaqModel;
import com.sioffice.service.FaqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/faq")
public class FaqController {
    @Qualifier("faqServiceImpl")
    @Autowired
    private FaqService faqService;

    @GetMapping("")
    public String daftarFaq(Model model) {
        List<FaqModel> daftarFaq = faqService.listFaq();
        model.addAttribute("daftarFaq", daftarFaq);
        model.addAttribute("query", "home");
        return "viewall-faq";
    }

    @GetMapping("/edit")
    public String editFaq(Model model) {
        List<FaqModel> daftarFaq = faqService.listFaq();
        model.addAttribute("daftarFaq", daftarFaq);
        model.addAttribute("query", "edit");
        return "viewall-faq";
    }

    @GetMapping("/hapus")
    public String hapusFaq(Model model) {
        List<FaqModel> daftarFaq = faqService.listFaq();
        model.addAttribute("daftarFaq", daftarFaq);
        model.addAttribute("query", "hapus");
        return "viewall-faq";
    }

    @GetMapping("/ubah-urutan")
    public String ubahUrutanFaq(Model model) {
        List<FaqModel> daftarFaq = faqService.listFaq();
        model.addAttribute("daftarFaq", daftarFaq);
        model.addAttribute("query", "ubah-urutan");
        return "viewall-faq";
    }

    @GetMapping("/tambah")
    public String FormTambahFaq(Model model) {
        FaqModel faq = new FaqModel();
        model.addAttribute("faq", faq);
        return "form-add-faq";
    }

    @PostMapping("/tambah")
    public String submitTambahFaq(Model model,
                                  @ModelAttribute FaqModel faq) {
        int jumlahFaq = faqService.listFaq().size();
        faq.setNoUrutan(jumlahFaq + 1);
        faqService.addFaq(faq);
        return "redirect:/faq";
    }

    @GetMapping("/edit/{id}")
    public String FormEditFaq(Model model,
                              @PathVariable(value = "id") Long id) {
        FaqModel faq = faqService.getFaqById(id);
        if (faq == null ){
            model.addAttribute("id", id);
            return "error";
        }
        model.addAttribute("faq", faq);
        return "form-edit-faq";
    }

    @PostMapping("/edit")
    public String submitEditFaq(Model model,
                                  @ModelAttribute FaqModel faq) {
        faqService.editFaq(faq);
        return "redirect:/faq/edit";
    }

    @PostMapping("hapus/{id}")
    public String submitHapusFaq(Model model,
                                 @PathVariable(value = "id") Long id) {
        FaqModel faq = faqService.getFaqById(id);
        faqService.deleteFaq(faq);
        faqService.updatePositionFaq(faq);
        return "redirect:/faq/hapus";
    }
}
