package com.sioffice.controller;

import com.sioffice.model.NodinModel;
import com.sioffice.service.NodinService;
import com.sioffice.service.PenggunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/arsip/nota-dinas")
public class ArsipNotaDinasController {
    @Qualifier("nodinServiceImpl")
    @Autowired
    private NodinService nodinService;

    @Qualifier("penggunaServiceImpl")
    @Autowired
    private PenggunaService penggunaService;

    @GetMapping(value = "")
    public String arsipNotaDinas(Model model,
                                 @RequestParam(value = "page", required = false) Integer page,
                                 @RequestParam(value = "size", required = false) Integer size) {
        if (page == null || size == null) {
            List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(0, 5);
            List<NodinModel> p = nodinService.getAllNotaDinas();
            int a = (int) Math.ceil((double) p.size()/5);
            model.addAttribute("daftarNodin", listNodin);
            model.addAttribute("countPage", a);
            model.addAttribute("size", 5);
            model.addAttribute("page", 1);
        } else {
            List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(page-1, size);
            List<NodinModel> p = nodinService.getAllNotaDinas();
            int a = (int) Math.ceil((double) p.size()/5);
            model.addAttribute("daftarNodin", listNodin);
            model.addAttribute("countPage", a);
            model.addAttribute("size", size);
            model.addAttribute("page", page);
        }
        model.addAttribute("type", "search-none");
        return "viewall-arsip-nota-dinas";
    }

    @GetMapping("/search")
    public String seachArsipNotaDinas(Model model,
                                      @RequestParam(value = "criteria") Optional<String> criteria,
                                      @RequestParam(value = "input", required = false) Optional<Object> inputSearch,
                                      @RequestParam(value = "page", required = false) Integer page,
                                      @RequestParam(value = "size", required = false) Integer size) {
        if (criteria.isPresent() && criteria.get().equals("perihal")) {
            String inputName = (String) inputSearch.get();
            model.addAttribute("type", "search-perihal");
            model.addAttribute("inputSearch", inputName);
            if (page == null || size == null) {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(inputName, 0, 5);
                List<NodinModel> p = nodinService.getAllNotaDinas(inputName);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
            } else {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(inputName, page-1, size);
                List<NodinModel> p = nodinService.getAllNotaDinas(inputName);
                int a = (int) Math.ceil((double) p.size()/size);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
            }
        } else if (criteria.isPresent() && criteria.get().equals("tanggal-pengajuan")) {
            LocalDate inputDate = LocalDate.parse((String) inputSearch.get());
            model.addAttribute("type", "search-tanggal-pengajuan");
            model.addAttribute("inputSearch", inputDate);
            if (page == null || size == null) {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(criteria.get(), inputDate, 0, 5);
                List<NodinModel> p = nodinService.getAllNotaDinas(criteria.get(), inputDate);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
            } else {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(criteria.get(), inputDate, page-1, size);
                List<NodinModel> p = nodinService.getAllNotaDinas(criteria.get(), inputDate);
                int a = (int) Math.ceil((double) p.size()/size);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
            }
        } else if (criteria.isPresent() && criteria.get().equals("tanggal-persetujuan-manajer")) {
            LocalDate inputDate = LocalDate.parse((String) inputSearch.get());
            model.addAttribute("type", "search-tanggal-persetujuan-manajer");
            model.addAttribute("inputSearch", inputDate);
            if (page == null || size == null) {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(criteria.get(), inputDate, 0, 5);
                List<NodinModel> p = nodinService.getAllNotaDinas(criteria.get(), inputDate);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
            } else {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(criteria.get(), inputDate, page-1, size);
                List<NodinModel> p = nodinService.getAllNotaDinas(criteria.get(), inputDate);
                int a = (int) Math.ceil((double) p.size()/size);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
            }

        } else if (criteria.isPresent() && criteria.get().equals("disetujui-direktur")) {
            model.addAttribute("type", "search-disetujui");
            if (page == null || size == null) {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(4, 0, 5);
                List<NodinModel> p = nodinService.getAllNotaDinas(4);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
            } else {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(4, page-1, size);
                List<NodinModel> p = nodinService.getAllNotaDinas(4);
                int a = (int) Math.ceil((double) p.size()/size);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
            }
        } else if (criteria.isPresent() && criteria.get().equals("ditolak-direktur")) {
            model.addAttribute("type", "search-ditolak");
            if (page == null || size == null) {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(5, 0, 5);
                List<NodinModel> p = nodinService.getAllNotaDinas(5);
                int a = (int) Math.ceil((double) p.size() / 5);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
            } else {
                List<NodinModel> listNodin = nodinService.getAllNotaDinasByPageAndSize(5, page - 1, size);
                List<NodinModel> p = nodinService.getAllNotaDinas(5);
                int a = (int) Math.ceil((double) p.size() / size);
                model.addAttribute("daftarNodin", listNodin);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
            }
        }
        return "viewall-arsip-nota-dinas";
    }
}
