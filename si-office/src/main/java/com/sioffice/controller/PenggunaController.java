package com.sioffice.controller;

import com.sioffice.model.*;
import com.sioffice.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/pengguna")
public class PenggunaController {
    @Qualifier("roleServiceImpl")
    @Autowired
    private RoleService roleService;

    @Qualifier("penggunaServiceImpl")
    @Autowired
    private PenggunaService penggunaService;

    @Qualifier("riwayatPendidikanServiceImpl")
    @Autowired
    private RiwayatPendidikanService riwayatPendidikanService;

    @Qualifier("kompetensiServiceImpl")
    @Autowired
    private KompetensiService kompetensiService;

    @Qualifier("keluargaServiceImpl")
    @Autowired
    private KeluargaService keluargaService;

    @GetMapping("/profil")
    private String profil(Model model) {
        Integer angka = 0;
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        if (pengguna.getFotoPengguna() != null) {
            angka = 1;
            model.addAttribute("id", pengguna.getFotoPengguna().getId());
        }
        model.addAttribute("pengguna", pengguna);
        model.addAttribute("angka", angka);
        return "lihat-profil";
    }

    @GetMapping("/updateProfil/{username}")
    public String FormUpdateProfil(@PathVariable String username,Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(username);

        model.addAttribute("pengguna", pengguna);
        return "form-edit-profil";
    }

    @PostMapping("/updateProfil")
    public String UpdateProfil(Model model,
                                      @ModelAttribute PenggunaModel pengguna) {
//        pengguna.setJml_tanggungan(penggunaService.jml_tanggungan(pengguna));
        PenggunaModel penggunaModel = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        pengguna.setFotoPengguna(penggunaModel.getFotoPengguna());
        pengguna.setUmur(penggunaService.hitung_umur(pengguna));
        penggunaService.updateProfil(pengguna);
        model.addAttribute("pengguna", pengguna);
        return "edit-profil";
    }

    @GetMapping("/kompetensi")
    private String viewkompetensi(Model model) {
        Integer angka = 0;
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        if (pengguna.getFotoPengguna() != null) {
            angka = 1;
            model.addAttribute("id", pengguna.getFotoPengguna().getId());
        }
        List<KompetensiModel> listKompetensi = pengguna.getListKompetensi();
        Integer jmlKomp = 0;
        if (listKompetensi.size() != 0) {
            jmlKomp = 1;
        }
        model.addAttribute("angka", angka);
        model.addAttribute("jmlKomp", jmlKomp);
        model.addAttribute("pengguna", pengguna);
        model.addAttribute("listKompetensi", listKompetensi);
        return "lihat-kompetensi";
    }
    @GetMapping("/tambahKompetensi")
    public String FormAddKompetensi(Model model) {
        return "form-add-kompetensi";
    }

    @PostMapping("/tambahKompetensi")
    public String submitAddKompetensi(Model model,
                                      @RequestParam(value = "topik") String topik,
                                      @RequestParam(value = "lokasi") String lokasi,
                                      @RequestParam(value = "tahun") String tahun,
                                      @RequestParam(value = "sertifikatFile") MultipartFile file) throws IOException {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        KompetensiModel kompetensi = new KompetensiModel();
        SertifikatEntity sertifikatFile = new SertifikatEntity();
        kompetensi.setPengguna(pengguna);
        kompetensi.setTopik(topik);
        kompetensi.setLokasi(lokasi);
        kompetensi.setTahun(tahun);
        kompetensi.setSertifikatFile(sertifikatFile);
        sertifikatFile.setKompetensi(kompetensi);
        kompetensiService.saveSertifikat(file, sertifikatFile);
        kompetensiService.addKompetensi(kompetensi);
        model.addAttribute("kompetensi", kompetensi);
        return "add-kompetensi";
    }

    @GetMapping("/updateKompetensi/{no_kompetensi}")
    public String FormUpdateKompetensi(@PathVariable Integer no_kompetensi, Model model) {
        KompetensiModel kompetensi = kompetensiService.getKompetensiById(no_kompetensi);
        model.addAttribute("kompetensi", kompetensi);
        return "form-edit-kompetensi";
    }

    @PostMapping("/updateKompetensi")
    public String submitUpdateKompetensi(Model model,
                                         @RequestParam(value = "no_kompetensi") Integer no_kompetensi,
                                         @RequestParam(value = "topik") String topik,
                                         @RequestParam(value = "lokasi") String lokasi,
                                         @RequestParam(value = "tahun") String tahun
                                         ){
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        KompetensiModel kompetensi = kompetensiService.getKompetensiById(no_kompetensi);
        kompetensi.setPengguna(pengguna);
        kompetensi.setTopik(topik);
        kompetensi.setLokasi(lokasi);
        kompetensi.setTahun(tahun);
        kompetensiService.updateKompetensi(kompetensi);
        model.addAttribute("kompetensi", kompetensi);
        return "edit-kompetensi";
    }

    @GetMapping("/uploadSertifikat/{no_kompetensi}")
    public String formUploadSertifikat(Model model, @PathVariable Integer no_kompetensi) {
        try {
            KompetensiModel kompetensi = kompetensiService.getKompetensiById(no_kompetensi);
            model.addAttribute("kompetensi", kompetensi);
            return "form-upload-sertifikat";
        } catch (Exception e) {
            model.addAttribute("message", "Error in getting image");
            return "redirect:/pengguna/kompetensi";
        }
    }

    @PostMapping("/uploadSertifikat")
    public String uploadSertifikat(Model model, @RequestParam Integer no_kompetensi,
                               @RequestParam(value = "sertifikatFile")MultipartFile file) throws IOException {
        SertifikatEntity sertifikatFile = new SertifikatEntity();
        KompetensiModel kompetensi = kompetensiService.getKompetensiById(no_kompetensi);
        kompetensi.setSertifikatFile(sertifikatFile);
        sertifikatFile.setKompetensi(kompetensi);
        kompetensiService.saveSertifikat(file, sertifikatFile);
        return "redirect:/pengguna/kompetensi";
    }

    @GetMapping("/delete/{no_kompetensi}")
    public String DeleteKompetensi(
            @PathVariable (value = "no_kompetensi", required = true) Integer no_kompetensi,
            @ModelAttribute("message") Object message,
            RedirectAttributes rd,
            Model model
    ){
        KompetensiModel kompetensi = kompetensiService.getKompetensiById(no_kompetensi);
        message = "Vendor " + kompetensi.getTopik() + " dengan ID " + kompetensi.getNo_kompetensi() + " telah berhasil dihapus";
        kompetensiService.deleteKompetensi(kompetensi);
        rd.addFlashAttribute("alertMessage", true);
        rd.addFlashAttribute("message", message);
        return "redirect:/pengguna/kompetensi";
    }

    @GetMapping("/riwayatPendidikan")
    private String riwayatPendidikan(Model model) {
        Integer angka =0;
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        if (pengguna.getFotoPengguna() != null) {
            angka = 1;
            model.addAttribute("id", pengguna.getFotoPengguna().getId());
        }

        List<RiwayatPendidikan> riwayatPendidikan = pengguna.getListPendidikan();
        Integer jmlPend = 0;
        if (riwayatPendidikan.size() != 0) {
            jmlPend = 1;
        }

        model.addAttribute("angka", angka);
        model.addAttribute("jmlPend", jmlPend);
        model.addAttribute("pengguna", pengguna);
        model.addAttribute("riwayatPendidikan", riwayatPendidikan);
        return "lihat-riwayatPendidikan";
    }

    @GetMapping("/tambahPendidikan")
    public String FormAddPendidikan(Model model) {
        return "form-add-pendidikan";
    }

    @PostMapping("/tambahPendidikan")
    public String submitAddPendidikan(Model model,
                                      @RequestParam(value = "jenjang") String jenjang,
                                      @RequestParam(value = "nama") String nama,
                                      @RequestParam(value = "tahun_lulus") String tahun_lulus,
                                      @RequestParam(value = "ijazahFile") MultipartFile file) throws IOException{
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        RiwayatPendidikan riwayatPendidikan = new RiwayatPendidikan();
        IjazahEntity ijazahFile = new IjazahEntity();
        riwayatPendidikan.setPengguna(pengguna);
        riwayatPendidikan.setJenjang(jenjang);
        riwayatPendidikan.setNama(nama);
        riwayatPendidikan.setTahun_lulus(tahun_lulus);
        ijazahFile.setRiwayatPendidikan(riwayatPendidikan);
        riwayatPendidikan.setIjazahFile(ijazahFile);
        riwayatPendidikanService.saveIjazah(file, ijazahFile);
        riwayatPendidikanService.addPendidikan(riwayatPendidikan);
        model.addAttribute("riwayatPendidikan", riwayatPendidikan);
        return "add-pendidikan";
    }

    @GetMapping("/updatePendidikan/{id}")
    public String FormUpdatePendidikan(@PathVariable Integer id, Model model) {
        RiwayatPendidikan riwayatPendidikan = riwayatPendidikanService.getPendidikanById(id);
        IjazahEntity ijazah = riwayatPendidikan.getIjazahFile();
        model.addAttribute("riwayatPendidikan", riwayatPendidikan);
        model.addAttribute("ijazahFile", ijazah);
        return "form-edit-pendidikan";
    }

    @PostMapping("/updatePendidikan")
    public String submitUpdatePendidikan(Model model,
                                         @RequestParam(value = "no_riwayat") Integer no_riwayat,
                                         @RequestParam(value = "jenjang") String jenjang,
                                         @RequestParam(value = "nama") String nama,
                                         @RequestParam(value = "tahun_lulus") String tahun_lulus) {

        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        RiwayatPendidikan riwayatPendidikan = riwayatPendidikanService.getPendidikanById(no_riwayat);
        riwayatPendidikan.setPengguna(pengguna);
        riwayatPendidikan.setJenjang(jenjang);
        riwayatPendidikan.setNama(nama);
        riwayatPendidikan.setTahun_lulus(tahun_lulus);
        riwayatPendidikanService.updatePendidikan(riwayatPendidikan);
        model.addAttribute("riwayatPendidikan", riwayatPendidikan);
        return "edit-pendidikan";
    }

    @GetMapping("/uploadIjazah/{no_riwayat}")
    public String formAddFoto(Model model, @PathVariable Integer no_riwayat) {
        try {
            RiwayatPendidikan riwayatPendidikan = riwayatPendidikanService.getPendidikanById(no_riwayat);
            model.addAttribute("riwayatPendidikan", riwayatPendidikan);
            return "form-upload-ijazah";
        } catch (Exception e) {
            model.addAttribute("message", "Error in getting image");
            return "redirect:/pengguna/riwayatPendidikan";
        }
    }

    @PostMapping("/uploadIjazah")
    public String uploadIjazah(Model model, @RequestParam Integer no_riwayat,
                           @RequestParam(value = "ijazahFile")MultipartFile file) throws IOException {
        IjazahEntity ijazahFile = new IjazahEntity();
        RiwayatPendidikan riwayatPendidikan = riwayatPendidikanService.getPendidikanById(no_riwayat);
        riwayatPendidikan.setIjazahFile(ijazahFile);
        ijazahFile.setRiwayatPendidikan(riwayatPendidikan);
        riwayatPendidikanService.saveIjazah(file, ijazahFile);
        return "redirect:/pengguna/riwayatPendidikan";
    }

    @GetMapping("/deletePendidikan/{no_riwayat}")
    public String DeletePendidikan(
            @PathVariable (value = "no_riwayat", required = true) Integer no_riwayat,
            @ModelAttribute("message") Object message,
            RedirectAttributes rd,
            Model model
    ){
        RiwayatPendidikan riwayatPendidikan = riwayatPendidikanService.getPendidikanById(no_riwayat);
        message = "Pendidikan " + riwayatPendidikan.getJenjang() + " dengan ID " + riwayatPendidikan.getNo_riwayat() + " telah berhasil dihapus";
        riwayatPendidikanService.deletePendidikan(riwayatPendidikan);
        rd.addFlashAttribute("alertMessage", true);
        rd.addFlashAttribute("message", message);
        return "redirect:/pengguna/riwayatPendidikan";
    }

    @GetMapping("/keluarga")
    private String viewKeluarga(Model model) {
        Integer angka = 0;
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        if (pengguna.getFotoPengguna() != null) {
            angka = 1;
            model.addAttribute("id", pengguna.getFotoPengguna().getId());
        }
        List<KeluargaModel> listKeluarga = pengguna.getListKeluarga();
        Integer jmlKel = 0;
        if (listKeluarga.size() != 0) {
            jmlKel = 1;
        }
        model.addAttribute("angka", angka);
        model.addAttribute("jmlKel", jmlKel);
        model.addAttribute("pengguna", pengguna);
        model.addAttribute("listKeluarga", listKeluarga);
        return "lihat-keluarga";
    }

    @GetMapping("/tambahKeluarga")
    public String FormAddKeluarga(Model model) {
        KeluargaModel keluarga = new KeluargaModel();

        model.addAttribute("keluarga", keluarga);
        return "form-add-keluarga";
    }

    @PostMapping("/tambahKeluarga")
    public String submitAddKeluarga(Model model,
                                      @ModelAttribute KeluargaModel keluarga) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );

        keluarga.setPengguna(pengguna);
        keluargaService.addKeluarga(keluarga);
        model.addAttribute("keluarga", keluarga);
        return "add-keluarga";
    }

    @GetMapping("/updateKeluarga/{id}")
    public String FormUpdateKeluarga(
            @PathVariable Integer id,
            Model model) {
        KeluargaModel keluarga = keluargaService.getKeluargaById(id);

        model.addAttribute("keluarga", keluarga);
        return "form-edit-keluarga";
    }

    @PostMapping("/updateKeluarga")
    public String UpdateKeluarga(
            @ModelAttribute KeluargaModel keluarga,
            Model model
    ){
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        keluarga.setPengguna(pengguna);
        model.addAttribute("keluarga", keluarga);
        keluargaService.updateKeluarga(keluarga);
        return "edit-keluarga";
    }

    @GetMapping("/deleteKeluarga/{noKeluarga}")
    public String DeleteKeluarga(
            @PathVariable (value = "noKeluarga", required = true) Integer noKeluarga,
            @ModelAttribute("message") Object message,
            RedirectAttributes rd,
            Model model
    ){
        KeluargaModel keluarga = keluargaService.getKeluargaById(noKeluarga);
        message = "Keluarga " + keluarga.getNama() + " dengan ID " + keluarga.getNoKeluarga() + " telah berhasil dihapus";
        keluargaService.deleteKeluarga(keluarga);
        rd.addFlashAttribute("alertMessage", true);
        rd.addFlashAttribute("message", message);
        return "redirect:/pengguna/keluarga";
    }

    @GetMapping("/ttd")
    public String formAddTtd(Model model) {
        Integer angka = 0;
        Integer foto = 0;
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        try {
            if (pengguna.getTtdPengguna() != null) {
                angka = 1;
                model.addAttribute("id", pengguna.getTtdPengguna().getId());
            }
            if (pengguna.getFotoPengguna() != null) {
                foto = 1;
                model.addAttribute("idFoto", pengguna.getFotoPengguna().getId());
            }
            model.addAttribute("pengguna", pengguna);
            model.addAttribute("angka", angka);
            model.addAttribute("foto", foto);
            return "form-add-tandatangan";
        } catch (Exception e) {
            model.addAttribute("message", "Error in getting image");
            return "redirect:/pengguna/profil";
        }
    }

    @PostMapping("/saveTtd")
    public String saveTtd(Model model,
                                  @RequestParam(value = "TtdPengguna")MultipartFile file) throws IOException {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        FileTtdEntity TtdPengguna = new FileTtdEntity();
        pengguna.setTtdPengguna(TtdPengguna);
        TtdPengguna.setPengguna(pengguna);
        penggunaService.saveTtdPengguna(file, TtdPengguna);
        return "redirect:/pengguna/ttd";
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getFileTtd(@PathVariable String id) {
        FileTtdEntity fileTtdEntity = penggunaService.getImages(id);

        if (fileTtdEntity==null) {
            return ResponseEntity.notFound()
                    .build();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileTtdEntity.getName() + "\"")
                .contentType(MediaType.valueOf(fileTtdEntity.getContentType()))
                .body(fileTtdEntity.getData());
    }

    @GetMapping("/foto")
    public String formAddFoto(Model model) {
        try {
            return "form-add-foto";
        } catch (Exception e) {
            model.addAttribute("message", "Error in getting image");
            return "redirect:/pengguna/profil";
        }
    }

    @PostMapping("/saveFoto")
    public String saveFoto(Model model,
                          @RequestParam(value = "FotoPengguna")MultipartFile file) throws IOException {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        FotoEntity fotoPengguna = new FotoEntity();
        pengguna.setFotoPengguna(fotoPengguna);
        fotoPengguna.setPengguna(pengguna);
        penggunaService.saveFotoPengguna(file, fotoPengguna);
        return "redirect:/pengguna/profil";
    }

    @GetMapping("/foto/{id}")
    public ResponseEntity<byte[]> getFileFoto(@PathVariable String id) {
        FotoEntity fotoEntity = penggunaService.getFoto(id);

        if (fotoEntity==null) {
            return ResponseEntity.notFound()
                    .build();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fotoEntity.getName() + "\"")
                .contentType(MediaType.valueOf(fotoEntity.getContentType()))
                .body(fotoEntity.getData());
    }

    @GetMapping("/ubahPassword")
    public String ubahPasswordFormPage(Model model) {

        return "form-ubah-password";
    }

    @PostMapping("/ubahPassword")
    public String ubahPasswordSubmitPage(
            @RequestParam(value = "oldPassword") String oldPassword,
            @RequestParam(value = "newPassword") String newPassword,
            @RequestParam(value = "confirmPassword") String confirmPassword,
            RedirectAttributes rd,
            Model model) {

        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        String error_message = "";
        String success_message = "";
        Boolean valid = penggunaService.validatePassword(pengguna, oldPassword);
        if (valid == true) {
            if (newPassword.equals(confirmPassword)) {
                pengguna.setPassword(newPassword);
                penggunaService.updatePassword(pengguna);
                return "ubah-password";
            }
            error_message = "Konfirmasi password tidak sesuai";
            model.addAttribute("error_message", error_message);
            return "form-ubah-password";
        }
        error_message = "Password Lama Belum Sesuai";
        model.addAttribute("error_message", error_message);
        return "form-ubah-password";
    }

    @GetMapping("/CV")
    public void downloadCV(HttpServletResponse response) throws IOException {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());

        response.setContentType("application/pdf");
        String headerKey = "CV";
        String headerValue = "attachment; filename=CV_" + pengguna.getUsername() + ".pdf";
        response.setHeader(headerKey, headerValue);

        CVModel cv = new CVModel();
        cv.downloadCV(response, pengguna);
    }

}
