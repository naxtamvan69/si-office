package com.sioffice.controller;

import com.sioffice.model.*;
import com.sioffice.service.RekananService;
import com.sioffice.service.PenggunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.io.IOException;

@Controller
@RequestMapping("/rekanan")
public class RekananController {
    @Qualifier("rekananServiceImpl")
    @Autowired
    private RekananService rekananService;

    @Qualifier("penggunaServiceImpl")
    @Autowired
    private PenggunaService penggunaService;

    @GetMapping("")
    public String listRekanan(Model model) {
        List<RekananModel> listRekanan = rekananService.listRekanan();
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        String divisi = "";
        if(role.equals("[Direktur]")){
            divisi = "Direktur";
        } else if(role.equals("[Manager]")){
            divisi = "Manager";
        } else if(role.equals("[Supervisor]")){
            divisi = "Supervisor";
        } else if(role.equals("[Karyawan]")){
            divisi = "Karyawan";
        } else if(role.equals("[Manager]")){
            divisi = "Manager";
        } else if(role.equals("[SDM]")){
            divisi = "SDM";
        } else if(role.equals("[Operasional]")){
            divisi = "Operasional";
        } else if(role.equals("[Marketing]")){
            divisi = "Marketing";
        } else if(role.equals("[Katering RS]")){
            divisi = "Katering RS";
        }
        model.addAttribute("role", role);
        model.addAttribute("divisi", divisi);
        model.addAttribute("listRekanan", listRekanan);
        return "viewall-rekanan";
    }

    @GetMapping("/tambah")
    public String FormAddRekanan(Model model) {
        return "form-add-rekanan";
    }

    @PostMapping("/add")
    public String SubmitRekanan(
            Model model,
            @RequestParam(value = "nama_rekanan") String nama_rekanan,
            @RequestParam(value = "alamat") String alamat,
            @RequestParam(value = "telp") String telp,
            @RequestParam(value = "profil_rekanan") String profil_rekanan,
            @RequestParam(value = "logorekanan") MultipartFile file) throws IOException
    {
        RekananModel rekanan = new RekananModel();
        LogoRekananEntity logorekanan = new LogoRekananEntity();
        rekanan.setNama_rekanan(nama_rekanan);
        rekanan.setAlamat(alamat);
        rekanan.setTelp(telp);
        rekanan.setProfil_rekanan(profil_rekanan);
        rekanan.setLogorekanan(logorekanan);
        logorekanan.setRekanan(rekanan);
        
        rekananService.addRekanan(rekanan);
        rekananService.saveLogoRekanan(file, logorekanan);
        return "add-rekanan";
    }

    @GetMapping("view/{id}")
    public String viewrekanan(
            @PathVariable Long id,
            Model model
    ){
        Integer foto = 0;
        RekananModel rekanan = rekananService.getRekananById(id);
        if(rekanan.getLogorekanan().getSize() != 0){
            foto = 1;
            model.addAttribute("id", rekanan.getLogorekanan().getId());
        }

        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        String divisi = "";
        if(role.equals("[Direktur]")){
            divisi = "Direktur";
        } else if(role.equals("[Manager]")){
            divisi = "Manager";
        } else if(role.equals("[Supervisor]")){
            divisi = "Supervisor";
        } else if(role.equals("[Karyawan]")){
            divisi = "Karyawan";
        } else if(role.equals("[Manager]")){
            divisi = "Manager";
        } else if(role.equals("[SDM]")){
            divisi = "SDM";
        } else if(role.equals("[Operasional]")){
            divisi = "Operasional";
        } else if(role.equals("[Marketing]")){
            divisi = "Marketing";
        } else if(role.equals("[Katering RS]")){
            divisi = "Katering RS";
        }
        
        model.addAttribute("role", role);
        model.addAttribute("divisi", divisi);
        model.addAttribute("rekanan", rekanan);
        model.addAttribute("foto", foto);
        return "view-rekanan";
    }

    @GetMapping("/view/foto/{id}")
    public ResponseEntity<byte[]> getFoto(@PathVariable String id){
        LogoRekananEntity logorekanan = rekananService.getLogoRekanan(id);
        if(logorekanan == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + logorekanan.getName() + "\"")
                .contentType(MediaType.valueOf(logorekanan.getContentType()))
                .body(logorekanan.getData());
    }

    @GetMapping("delete/{id}")
    public String DeleteRekanan(
            @PathVariable (value = "id", required = true) Long id,
            RedirectAttributes rd,
            Model model
    ){
        RekananModel rekanan = rekananService.getRekananById(id);
        rekananService.deleteRekanan(rekanan);
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("role", role);
        return "redirect:/rekanan";
    }

    @GetMapping("/edit/{id}")
    public String FormEditRekanan(
        Model model,
        @PathVariable(value = "id") Long id
    ) {
        RekananModel rekanan = rekananService.getRekananById(id);
        //LogoRekananEntity logorekanan = rekanan.getLogorekanan();
        
        model.addAttribute("rekanan", rekanan);
        return "form-edit-rekanan";
    }

    @PostMapping("/edit")
    public String EditRekanan(
            Model model,
            @RequestParam(value = "idRekanan") Long idRekanan,
            @RequestParam(value = "nama_rekanan") String nama_rekanan,
            @RequestParam(value = "alamat") String alamat,
            @RequestParam(value = "telp") String telp,
            @RequestParam(value = "profil_rekanan") String profil_rekanan,
            @RequestParam(value = "logorekanan")MultipartFile file) throws IOException
    {
        RekananModel rekanan = rekananService.getRekananById(idRekanan);
        LogoRekananEntity logorekanan = rekanan.getLogorekanan();
        rekanan.setNama_rekanan(nama_rekanan);
        rekanan.setAlamat(alamat);
        rekanan.setTelp(telp);
        rekanan.setProfil_rekanan(profil_rekanan);
        rekanan.setLogorekanan(logorekanan);
        logorekanan.setRekanan(rekanan);

        rekananService.editRekanan(rekanan);
        rekananService.editLogoRekanan(file, logorekanan);
        model.addAttribute("rekanan", rekanan);
        return "edit-rekanan";
    }
}