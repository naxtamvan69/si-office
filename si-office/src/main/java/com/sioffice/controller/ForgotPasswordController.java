package com.sioffice.controller;

import com.sioffice.model.PenggunaModel;
import com.sioffice.service.PenggunaService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Controller
public class ForgotPasswordController {
    @Autowired
    private JavaMailSender mailSender;

    @Qualifier("penggunaServiceImpl")
    @Autowired
    private PenggunaService penggunaService;

    @GetMapping("/forgot_password")
    public String showForgotPasswordForm() {
        return "forgot_password_form";
    }

    @PostMapping("/forgot_password")
    public String processForgotPassword(HttpServletRequest request,
                                        Model model) {
        String email = request.getParameter("email");
        String token = RandomString.make(30);

        Integer validateEmail = penggunaService.checkEmail(email);

        try {
            if (validateEmail == 1) {
                penggunaService.updateResetPasswordToken(token, email);
                String resetPasswordLink = Utility.getSiteURL(request) + "/reset_password?token=" + token;
                sendEmail(email, resetPasswordLink);
                model.addAttribute("message", "We have sent a reset password link to your email. Please check your spam email.");

            } else {
                model.addAttribute("error", "Sorry, the email you are using is not correct. please enter the email registered on the system.");
            }

        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", "Error while sending email");
        }

        return "forgot_password_form";
    }

    public void sendEmail (String recipientEmail, String link)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("contact@shopme.com", "no-reply");
        helper.setTo(recipientEmail);

        String subject = "Here's the link to reset your password";

        String content = "<p>Hello, We are from Si-Office Orchid System</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + link + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);

    }


    @GetMapping("/reset_password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
        PenggunaModel pengguna = penggunaService.getByResetPasswordToken(token);
        model.addAttribute("token", token);

        if (pengguna == null) {
            model.addAttribute("message", "Invalid Token");
            return "message";
        }

        return "reset_password_form";

    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request,
                                       Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");

        PenggunaModel pengguna = penggunaService.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");

        if (pengguna == null) {
            model.addAttribute("message", "Invalid Token");
            return "message";
        } else {
            if (password.equals(confirmPassword)) {
                penggunaService.updatePassword(pengguna, password);
            } else {
                model.addAttribute("error", "Sorry, the password confirmation is not correct");
                return "reset_password_form";
            }
        }

        return "success_reset_password";

    }

}

class Utility {
    public static String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }
}
