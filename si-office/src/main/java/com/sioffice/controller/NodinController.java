package com.sioffice.controller;

import com.sioffice.model.NodinModel;
import com.sioffice.model.ModelToPDF;
import com.sioffice.model.PenggunaModel;
import com.sioffice.service.NodinService;
import com.sioffice.service.PenggunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("/notadinas")
public class NodinController {
    @Qualifier("nodinServiceImpl")
    @Autowired
    private NodinService nodinService;

    @Qualifier("penggunaServiceImpl")
    @Autowired
    private PenggunaService penggunaService;

    @GetMapping("")
    public String showAllNodin(Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        if (pengguna.getRole().getNama_role().equals("Operasional") || pengguna.getRole().getNama_role().equals("Marketing")) {
            model.addAttribute("nodinList", nodinService.getAllNotaDinas(pengguna));
        } else if (pengguna.getRole().getNama_role().equals("Manager")) {
            model.addAttribute("nodinList", nodinService.getAllNotaDinas(1));
        } else if (pengguna.getRole().getNama_role().equals("Direktur")) {
            model.addAttribute("nodinList", nodinService.getAllNotaDinas(2));
        }
        return "viewall-nodin";
    }

    @GetMapping(value = "/detail/{nomor}")
    private String detailNodin(@PathVariable Long nomor, Model model) {
        NodinModel nodin = nodinService.getANotaDinas(nomor);
        model.addAttribute("nodin", nodin);

        if (nodin.getStatus() != 0) {
            LocalDate tanggal = nodin.getTanggalPengajuan();
            String teksTanggal = DateTimeFormatter.ofPattern("dd MMM yyyy").format(tanggal);
            model.addAttribute("tanggal", teksTanggal);
        }


        return "view-nodin";
    }

    @GetMapping(value = "/add")
    private String addNodinForm(Model model) {
        NodinModel nodin = new NodinModel();
        model.addAttribute("nodin", nodin);
        return "form-add-nodin";
    }

    @PostMapping(value = "/add")
    private String addNodin(@ModelAttribute NodinModel nodin, RedirectAttributes redirectAttributes) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        nodin.setPengguna(pengguna);
        nodinService.saveNodin(nodin);
        redirectAttributes.addAttribute("nomor", nodin.getNomor());
        return "redirect:/notadinas/detail/{nomor}";
    }

    @GetMapping(value = "/edit/{nomor}")
    private String editNodinForm(@PathVariable Long nomor, Model model) {
        NodinModel nodin = nodinService.getANotaDinas(nomor);
        model.addAttribute("nodin", nodin);
        return "form-edit-nodin";
    }

    @PostMapping(value = "/edit")
    private String editNodin(@ModelAttribute NodinModel nodin, RedirectAttributes redirectAttributes) {
        if (nodin.getAlasanPenolakan() == "") nodin.setAlasanPenolakan(null);
        if (nodin.getDisposisi() == "") nodin.setDisposisi(null);
        
        nodinService.saveNodin(nodin);
        redirectAttributes.addAttribute("nomor", nodin.getNomor());
        return "redirect:/notadinas/detail/{nomor}";
    }

    @GetMapping("/hapus/{nomor}")
    public String hapusNodin(@PathVariable Long nomor) {
        NodinModel nodin = nodinService.getANotaDinas(nomor);
        nodinService.deleteNotaDinas(nodin);
        return "redirect:/notadinas";
    }

    @GetMapping("/ajukan/{nomor}")
    public String ajukanNodin(@PathVariable Long nomor, Model model) {
        NodinModel nodin = nodinService.getANotaDinas(nomor);
        nodin.setStatus(1);
        nodin.setTanggalPengajuan(LocalDate.now());
        nodinService.saveNodin(nodin);
        model.addAttribute("msg", "Surat telah Diajukan untuk Mendapatkan Persetujuan");
        return "nodin-confirmation-page";
    }

    @GetMapping("/unduh/{nomor}")
    public void exportToPDF(HttpServletResponse response, @PathVariable Long nomor) throws IOException {
        NodinModel nodin = nodinService.getANotaDinas(nomor);
        PenggunaModel manajer = penggunaService.getPenggunaByUsername("276060305");

        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=nota_dinas_" + nodin.getNomor() + ".pdf";
        response.setHeader(headerKey, headerValue);

        ModelToPDF exporter = new ModelToPDF();
        exporter.exportNodin(response, nodin, manajer);
    }

    @GetMapping("/setujui/{nomor}")
    public String setujuiNodin(@PathVariable Long nomor, Model model) {
        NodinModel nodin = nodinService.getANotaDinas(nomor);
        nodin.setStatus(2);
        nodin.setTanggalPersetujuanManajer(LocalDate.now());
        nodin.setAlasanPenolakan(null);
        nodinService.saveNodin(nodin);
        model.addAttribute("msg", "Surat Telah Anda Setujui untuk Diteruskan ke Direktur");
        return "nodin-confirmation-page";
    }

    @GetMapping("/tolak/{nomor}")
    public String tolakNodin(@PathVariable Long nomor, Model model) {
        NodinModel nodin = nodinService.getANotaDinas(nomor);
        model.addAttribute("nodin", nodin);
        return "alasan-penolakan-nodin";
    }

    @PostMapping(value = "/tolak")
    private String tolakNodinConfirmed(@ModelAttribute NodinModel nodin, Model model) {
        nodin.setStatus(3);
        nodinService.saveNodin(nodin);
        model.addAttribute("msg", "Surat Telah Anda Tolak dan Akan Dikembalikan ke Pembuat");
        return "nodin-confirmation-page";
    }

    @GetMapping("/disposisi/{status}/{nomor}")
    public String disposisiNodin(@PathVariable String status, @PathVariable Long nomor, Model model) {
        NodinModel nodin = nodinService.getANotaDinas(nomor);
        model.addAttribute("nodin", nodin);
        model.addAttribute("currentStatus", status);

        LocalDate tanggal = nodin.getTanggalPengajuan();
        String teksTanggal = DateTimeFormatter.ofPattern("dd MMM yyyy").format(tanggal);
        model.addAttribute("tanggal", teksTanggal);
        return "disposisi-nodin";
    }

    @PostMapping(value = "/disposisi/{status}")
    private String disposisiNodinConfirmed(@PathVariable String status, @ModelAttribute NodinModel nodin, Model model) {
        nodin.setStatus(Integer.valueOf(status));
        nodinService.saveNodin(nodin);
        model.addAttribute("msg", "Disposisi yang Anda Berikan akan Diteruskan ke Pembuat");
        return "nodin-confirmation-page";
    }


}
