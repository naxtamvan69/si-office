package com.sioffice.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;


@Controller
public class PageController {
    @GetMapping(value = "/")
    public String home(){
        return "home";
    }
    
    @RequestMapping("/login")
    public String login(){
        return "login";
    }

}
