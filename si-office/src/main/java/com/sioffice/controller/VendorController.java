package com.sioffice.controller;
import com.sioffice.model.*;
import com.sioffice.service.KaryawanService;
import com.sioffice.service.PenggunaService;
import com.sioffice.service.ProdukVendorService;
import com.sioffice.service.VendorService;
import org.hibernate.persister.entity.SingleTableEntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/vendor")
public class VendorController {
    @Qualifier("vendorServiceImpl")
    @Autowired
    private VendorService vendorService;

    @Qualifier("produkVendorServiceImpl")
    @Autowired
    private ProdukVendorService produkVendorService;

    @Qualifier("penggunaServiceImpl")
    @Autowired
    private PenggunaService penggunaService;

    @GetMapping("")
    public String listVendor(Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        String role = pengguna.getRole().getNama_role();
        List<VendorModel> listVendor = vendorService.listVendor();
        model.addAttribute("role", role);
        model.addAttribute("pengguna", pengguna);
        model.addAttribute("listVendor", listVendor);
        return "viewall-vendor";
    }

    @GetMapping("/tambah")
    public String FormAddVendor(Model model) {
        VendorModel vendor = new VendorModel();
        model.addAttribute("vendor", vendor);
        return "form-add-vendor";
    }

    @PostMapping("/tambah")
    public String submitAddVendor(Model model,
                                  @ModelAttribute VendorModel vendor) {
        Integer jml_produk = 0;
        if (vendor.getListProduk() != null) {
            jml_produk = vendor.getListProduk().size();
        }
        vendor.setJumlah_produk(jml_produk);
        vendorService.addVendor(vendor);
        model.addAttribute("vendor", vendor);
        return "add-vendor";
    }

    @GetMapping("/update/{id}")
    public String FormUpdateVendor(
            @PathVariable Integer id,
            Model model) {
        VendorModel vendor = vendorService.getVendorById(id);

        model.addAttribute("vendor", vendor);
        return "form-edit-vendor";
    }

    @PostMapping("/update")
    public String UpdateVendor(
            @ModelAttribute VendorModel vendor,
            Model model
    ) {
        vendorService.updateVendor(vendor);
        model.addAttribute("id", vendor.getId());
        return "edit-vendor";
    }

    @GetMapping("/delete/{id}")
    public String DeleteVendor(
            @PathVariable(value = "id", required = true) Integer id,
            @ModelAttribute("message") Object message,
            RedirectAttributes rd,
            Model model
    ) {
        VendorModel vendor = vendorService.getVendorById(id);
        message = "Vendor " + vendor.getNama_vendor() + " dengan ID " + vendor.getId() + " telah berhasil dihapus";
        vendorService.deleteVendor(vendor);
        rd.addFlashAttribute("alertMessage", true);
        rd.addFlashAttribute("message", message);
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("role", role);
        return "redirect:/vendor";
    }

    @GetMapping("/view/{id}")
    public String viewVendor(
            @PathVariable Integer id,
            Model model
    ) {
        VendorModel vendor = vendorService.getVendorById(id);
        List<ProdukVendorModel> listProduk = vendor.getListProduk();
        Integer jml_produk = 0;
        if (vendor.getListProduk() != null) {
            jml_produk = vendor.getListProduk().size();
        }
        vendor.setJumlah_produk(jml_produk);
        model.addAttribute("vendor", vendor);
        model.addAttribute("listProduk", listProduk);
        model.addAttribute("id", id);

        return "view-vendor";
    }

    @GetMapping("/tambahProduk/{id}")
    public String FormAddProduk(Model model, @PathVariable Integer id) {
        ProdukVendorModel produkVendor = new ProdukVendorModel();
        VendorModel vendor = vendorService.getVendorById(id);
        produkVendor.setVendor(vendor);
        model.addAttribute("vendor", vendor);
        model.addAttribute("produkVendor", produkVendor);
        model.addAttribute("id", id);
        return "form-add-produk";
    }

    @PostMapping("/tambahProduk")
    public String submitAddProduk(Model model,
                                  @ModelAttribute ProdukVendorModel produkVendor) {

        produkVendorService.addProdukVendor(produkVendor);
        model.addAttribute("produkVendor", produkVendor);
        model.addAttribute("id", produkVendor.getVendor().getId());
        return "add-produk";
    }

    @GetMapping("/updateProduk/{id}")
    public String FormUpdateProduk(Model model, @PathVariable Integer id) {
        ProdukVendorModel produkVendor = produkVendorService.getProdukVendorById(id);
        model.addAttribute("produkVendor", produkVendor);
        model.addAttribute("id", id);
        return "form-edit-produk";
    }

    @PostMapping("/updateProduk")
    public String submitUpdateProduk(Model model,
                                  @ModelAttribute ProdukVendorModel produkVendor) {
        produkVendorService.addProdukVendor(produkVendor);
        model.addAttribute("produkVendor", produkVendor);
        model.addAttribute("id", produkVendor.getVendor().getId());
        return "edit-produk";
    }

    @GetMapping("/deleteProduk/{id}")
    public String DeleteProduk(
            @PathVariable(value = "id", required = true) Integer id,
            @ModelAttribute("message") Object message,
            RedirectAttributes rd,
            Model model
    ) {
        ProdukVendorModel produkVendor = produkVendorService.getProdukVendorById(id);
        message = "Produk Vendor " + produkVendor.getProduk() + " dengan ID " + produkVendor.getId() + " telah berhasil dihapus";
        produkVendorService.deleteProdukVendor(produkVendor);
        rd.addFlashAttribute("alertMessage", true);
        rd.addFlashAttribute("message", message);
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("role", role);
        return "redirect:/vendor/view/" + produkVendor.getVendor().getId();
    }

    @RequestMapping(path = {"/search"})
    public String search(VendorModel vendor, Model model, String keyword) {
        if(keyword!=null) {
            List<VendorModel> listVendor = vendorService.getByKeyword(keyword);
            model.addAttribute("listVendor", listVendor);
        }else {
            List<VendorModel> listVendor = vendorService.listVendor();
            model.addAttribute("listVendor", listVendor);}
        return "viewall-vendor";
    }
}
