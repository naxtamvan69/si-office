package com.sioffice.controller;

import com.sioffice.model.*;
import com.sioffice.service.BeritaService;
import com.sioffice.service.CutiService;
import com.sioffice.service.PenggunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/berita")
public class BeritaController {

    @Qualifier("penggunaServiceImpl")
    @Autowired
    private PenggunaService penggunaService;

    @Qualifier("beritaServiceImpl")
    @Autowired
    private BeritaService beritaService;

    @GetMapping("")
    public String viewallBerita(Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        List<BeritaModel> listBerita = beritaService.getListBerita();
        model.addAttribute("listBerita",listBerita);
        return "viewall-berita";
    }

    @GetMapping("/buat")
    public String buatBerita(Model model){
        BeritaModel berita = new BeritaModel();

        model.addAttribute("berita",berita);
        return "form-add-berita";
    }

    @PostMapping("/buat")
    public String buatBeritaSubmit(@ModelAttribute BeritaModel berita,Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        berita.setPembuat(pengguna);
//        berita.setIsi(berita.getIsi().replace("\n", "<br />\n"));
        beritaService.addBerita(berita);
        return "add-berita";
    }

    @GetMapping("/hapus/{noBerita}")
    public String hapusBerita(
            @PathVariable(value = "noBerita") long noBerita,
            Model model
    ) {
        BeritaModel berita = beritaService.getBeritaByNoBerita(noBerita);
        beritaService.delete(berita);
        return "hapus-berita";
    }

    @GetMapping("/ubah/{noBerita}")
    public String ubahBeritaForm(
            @PathVariable(value = "noBerita") long noBerita,
            Model model
    ) {
        BeritaModel berita = beritaService.getBeritaByNoBerita(noBerita);
        model.addAttribute("berita",berita);
        return "form-update-berita";
    }

    @PostMapping("/ubah")
    public String ubahBerita(
            @ModelAttribute BeritaModel berita,
            Model model
    ) {
        beritaService.save(berita);
        return "update-berita";
    }
}
