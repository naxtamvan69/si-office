package com.sioffice.controller;

import com.sioffice.model.FileSuratMasukEntity;
import com.sioffice.model.SuratMasukModel;
import com.sioffice.service.SuratMasukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/arsip/surat-masuk")
public class SuratMasukController {
    @Qualifier("suratMasukServiceImpl")
    @Autowired
    private SuratMasukService suratMasukService;

    @GetMapping("")
    public String daftarSuratMasuk(Model model,
                                   @RequestParam(value = "page", required = false) Integer page,
                                   @RequestParam(value = "size", required = false) Integer size) {
        if (page == null || size == null) {
            List<SuratMasukModel> daftarSuratMasuk = suratMasukService.getListSuratMasuk();
            int a = (int) Math.ceil((double) daftarSuratMasuk.size()/5);
            daftarSuratMasuk = suratMasukService.getListSuratMasukByPage(0, 5);
            model.addAttribute("daftarSuratMasuk", daftarSuratMasuk);
            model.addAttribute("countPage", a);
            model.addAttribute("size", 5);
            model.addAttribute("page", 1);
        } else {
            List<SuratMasukModel> daftarSuratMasuk = suratMasukService.getListSuratMasuk();
            int a = (int) Math.ceil((double) daftarSuratMasuk.size()/size);
            daftarSuratMasuk = suratMasukService.getListSuratMasukByPage(page-1, size);
            model.addAttribute("daftarSuratMasuk", daftarSuratMasuk);
            model.addAttribute("countPage", a);
            model.addAttribute("size", size);
            model.addAttribute("page", page);
        }

        model.addAttribute("type", "search-none");
        return "viewall-surat-masuk";
    }

    @GetMapping("/unggah")
    public String formUnggahSuratMasuk(Model model) {
        return "form-unggah-surat-masuk";
    }

    @PostMapping("/unggah")
    public String submitUnggahSuratMasuk(Model model,
                                         @RequestParam(value = "nomor") String nomor,
                                         @RequestParam(value = "perihal") String perihal,
                                         @RequestParam(value = "pengirim") String pengirim,
                                         @RequestParam(value = "penerima") String penerima,
                                         @RequestParam(value = "fileSuratMasuk") MultipartFile file) throws IOException {
        SuratMasukModel suratMasuk = new SuratMasukModel();
        FileSuratMasukEntity fileSuratMasuk = new FileSuratMasukEntity();
        suratMasuk.setNomor(nomor);
        suratMasuk.setPerihal(perihal);
        suratMasuk.setTanggal(LocalDate.now());
        suratMasuk.setPengirim(pengirim);
        suratMasuk.setPenerima(penerima);
        suratMasuk.setFileSuratMasuk(fileSuratMasuk);
        fileSuratMasuk.setSuratMasuk(suratMasuk);
        suratMasukService.addSuratMasuk(suratMasuk);
        suratMasukService.saveFileSuratMasuk(file, fileSuratMasuk);
        return "redirect:/arsip/surat-masuk";
    }

    @GetMapping("/search")
    public String searchSuratMasuk(Model model,
                                 @RequestParam(value = "criteria") Optional<String> criteria,
                                 @RequestParam(value = "input") Optional<Object> inputSearch,
                                   @RequestParam(value = "page", required = false) Integer page,
                                   @RequestParam(value = "size", required = false) Integer size) {
        String inputName;
        LocalDate inputDate;
        List<SuratMasukModel> daftarSuratMasuk = new ArrayList<>();
        if (page == null || size == null) {
            if (criteria.isPresent() && criteria.get().equals("perihal")) {
                inputName = (String) inputSearch.get();
                daftarSuratMasuk = suratMasukService.getListSuratMasukByPerihalAndPageAndSize(inputName, 0, 5);
                List<SuratMasukModel> p = suratMasukService.getListSuratMasukByPerihal(inputName);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
                model.addAttribute("type", "search-perihal");
                model.addAttribute("inputSearch", inputName);
            } else if (criteria.isPresent() && criteria.get().equals("tanggal")) {
                inputDate = LocalDate.parse((String) inputSearch.get());
                daftarSuratMasuk = suratMasukService.getListSuratMasukByDateAndPageAndSize(inputDate, 0, 5);
                List<SuratMasukModel> p = suratMasukService.getListSuratMasukByDate(inputDate);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
                model.addAttribute("type", "search-tanggal");
                model.addAttribute("inputSearch", inputDate);
            }
        } else {
            if (criteria.isPresent() && criteria.get().equals("perihal")) {
                inputName = (String) inputSearch.get();
                daftarSuratMasuk = suratMasukService.getListSuratMasukByPerihalAndPageAndSize(inputName, page-1, size);
                List<SuratMasukModel> p = suratMasukService.getListSuratMasukByPerihal(inputName);
                int a = (int) Math.ceil((double) p.size()/size);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
                model.addAttribute("type", "search-perihal");
                model.addAttribute("inputSearch", inputName);
            } else if (criteria.isPresent() && criteria.get().equals("tanggal")) {
                inputDate = LocalDate.parse((String) inputSearch.get());
                daftarSuratMasuk = suratMasukService.getListSuratMasukByDateAndPageAndSize(inputDate, page-1, size);
                List<SuratMasukModel> p = suratMasukService.getListSuratMasukByDate(inputDate);
                int a = (int) Math.ceil((double) p.size()/size);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
                model.addAttribute("type", "search-tanggal");
                model.addAttribute("inputSearch", inputDate);
            }
        }
        model.addAttribute("daftarSuratMasuk", daftarSuratMasuk);
        return "viewall-surat-masuk";
    }

    @GetMapping("/edit/{id}")
    public String formEditSuratMasuk(Model model, @PathVariable(value = "id") Long id) {
        SuratMasukModel suratMasuk = suratMasukService.getSuratMasuk(id);
        FileSuratMasukEntity fileSuratMasuk = suratMasuk.getFileSuratMasuk();
        model.addAttribute("suratMasuk", suratMasuk);
        return "form-edit-surat-masuk";
    }

    @PostMapping("/edit")
    public String submitEditSuratMasuk(Model model,
                                     @RequestParam(value = "idSuratMasuk") Long idSuratMasuk,
                                     @RequestParam(value = "nomor") String nomor,
                                     @RequestParam(value = "perihal") String perihal,
                                       @RequestParam(value = "pengirim") String pengirim,
                                       @RequestParam(value = "penerima") String penerima,
                                     @RequestParam(value = "selectFile") String selectFile,
                                     @RequestParam(value = "fileSuratMasuk", required = false)MultipartFile file) throws IOException {
        SuratMasukModel suratMasuk = suratMasukService.getSuratMasuk(idSuratMasuk);
        FileSuratMasukEntity fileSuratMasuk = suratMasuk.getFileSuratMasuk();
        suratMasuk.setNomor(nomor);
        suratMasuk.setPerihal(perihal);
        suratMasuk.setTanggal(LocalDate.now());
        suratMasuk.setPengirim(pengirim);
        suratMasuk.setPenerima(penerima);
        if (selectFile.equals("new-file")) {
            suratMasuk.setFileSuratMasuk(fileSuratMasuk);
            fileSuratMasuk.setSuratMasuk(suratMasuk);
            suratMasukService.editSuratMasuk(suratMasuk);
            suratMasukService.editFileSuratMasuk(file, fileSuratMasuk);
        } else {
            suratMasukService.editSuratMasuk(suratMasuk);
        }
        return "redirect:/arsip/surat-masuk";
    }
}
