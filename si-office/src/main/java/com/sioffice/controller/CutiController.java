package com.sioffice.controller;

import com.sioffice.model.*;
import com.sioffice.service.CutiService;
import com.sioffice.service.PenggunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/cuti")
public class CutiController {
    @Qualifier("penggunaServiceImpl")
    @Autowired
    private PenggunaService penggunaService;

    @Qualifier("cutiServiceImpl")
    @Autowired
    private CutiService cutiService;

    @Transactional
    @GetMapping("")
    public String viewallCuti(Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        List<CutiModel> listCuti = pengguna.getListCuti();
        int listCutiRiwayat = 0;
        int listCutiPending = 0;
        for(CutiModel c: listCuti){
            if(c.getStatus().equals("Menunggu Persetujuan Supervisi") || c.getStatus().equals("Menunggu Persetujuan Manager")
            || c.getStatus().equals("Menunggu Persetujuan Direktur")){
                listCutiPending++;
            } else listCutiRiwayat++;
        }
        
        model.addAttribute("userz", pengguna);
        model.addAttribute("listCutiRiwayat", listCutiRiwayat);
        model.addAttribute("listCutiPending", listCutiPending);
        model.addAttribute("listCuti", listCuti);
        return "viewall-cuti";
    }

    @Transactional
    @GetMapping("/daftar-pengajuan")
    public String viewallPengajuanCuti(Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );

        List<CutiModel> listCuti = new ArrayList<>();
        List<CutiModel> listCutiKelola = new ArrayList<>();
        List<CutiModel> listAllCuti = cutiService.listCuti();

        boolean isempty = true;

        if(pengguna.getRole().getNama_role().equals("SDM")){
            listCuti = listAllCuti;
            isempty = false;
        } else {
            for(CutiModel c: listAllCuti){
                if(pengguna.getRole().getNama_role().equals("Supervisor") && c.getStatus().equals("Menunggu Persetujuan Supervisi")){
                    if(c.getPengguna().getDivisi().equals(pengguna.getDivisi())){
                        listCuti.add(c);
                        isempty = false;
                    }
                } else if(pengguna.getRole().getNama_role().equals("Manager") && c.getStatus().equals("Menunggu Persetujuan Manager")){
                    listCuti.add(c);
                    isempty = false;
                } else if(pengguna.getRole().getNama_role().equals("Direktur") && c.getStatus().equals("Menunggu Persetujuan Direktur")){
                    listCuti.add(c);
                    isempty = false;
                }
            }
        }

        model.addAttribute("pengguna", pengguna);
        model.addAttribute("isempty", isempty);
        model.addAttribute("listCuti", listCuti);
        return "viewall-pengajuan-cuti";
    }

    @GetMapping(value = "/add")
    public String addCutiForm(Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );

        CutiModel cuti = new CutiModel();
        Integer durasi = pengguna.getSisa_cuti();
        List<CutiModel> listCuti = cutiService.listCuti();
        List<LocalDate> forbiddenDates = new ArrayList<>();
        for(CutiModel c: listCuti){
            if((c.getPengguna().getDivisi()).equals(pengguna.getDivisi())){
                if(!(c.getStatus().equals("Dibatalkan")) && !(c.getStatus().equals("Ditolak"))){
                    List<LocalDate> generatedDates =cutiService.generateDates(c.getTanggal_mulai(),c.getTanggal_akhir());
                    for(int i = 0; i<generatedDates.size(); i++){
                        forbiddenDates.add(generatedDates.get(i));
                    }
                }
            }
        }
        model.addAttribute("forbiddenDates", forbiddenDates);
        model.addAttribute("durasi", durasi);
        model.addAttribute("cuti", cuti);
        return "form-add-cuti";
    }

    
    @PostMapping(value = "/add")
    public String addCuti(@ModelAttribute CutiModel cuti, Model model) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        cuti.setPengguna(pengguna);
        
        //Hitung durasi by given date
        long durasi = (long) cutiService.workingDays(cuti.getTanggal_mulai(), cuti.getTanggal_akhir());
        cuti.setDurasi(durasi);
        
        //simpan cuti
        cutiService.addCuti(cuti);

        if(pengguna.getRole().getNama_role().equals("Supervisor")){
            cuti.setStatus("Menunggu Persetujuan Manager");
            cutiService.save(cuti);
        } else if(pengguna.getRole().getNama_role().equals("Manager")){
            cuti.setStatus("Menunggu Persetujuan Direktur");
            cutiService.save(cuti);
        }

        //Kurangi sisa cuti pengguna
        if(cuti.getJenis().equals("Cuti Tahunan")){
            cuti.getPengguna().setSisa_cuti(cuti.getPengguna().getSisa_cuti() - (int) durasi);
        }
        
        penggunaService.save(pengguna);

        model.addAttribute("msg", "Pengajuan cuti Anda sedang diproses. Cek lagi dalam 2-5 hari kerja.");
//        model.addAttribute("durasi", durasi);
        return "add-cuti";
    }

    @GetMapping("/{noCuti}")
    public String detailCuti(
            @PathVariable(value = "noCuti") long noCuti,
            Model model
    ) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        CutiModel cuti = cutiService.getCutiByNoCuti(noCuti);
        model.addAttribute("cuti", cuti);
        model.addAttribute("pengguna", pengguna);
        return "view-cuti";
    }

    @GetMapping("/pengajuan/{noCuti}")
    public String detailKelolaCuti(
            @PathVariable(value = "noCuti") long noCuti,
            Model model
    ) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        CutiModel cuti = cutiService.getCutiByNoCuti(noCuti);
        model.addAttribute("cuti", cuti);
        model.addAttribute("pengguna", pengguna);
        return "view-pengajuan-cuti";
    }

    @GetMapping("/batal/{noCuti}")
    public String batalCuti(
            @PathVariable(value = "noCuti") long noCuti,
            Model model
    ) {
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );

        CutiModel cuti = cutiService.getCutiByNoCuti(noCuti);

        cuti.setStatus("Dibatalkan");

        if(cuti.getJenis().equals("Cuti Tahunan")){
            int hasilTambah = cuti.getPengguna().getSisa_cuti() + (int) cuti.getDurasi();
            if(hasilTambah > 12){
                hasilTambah=12;
            } else {
                cuti.getPengguna().setSisa_cuti(hasilTambah);
            }
        }
        penggunaService.save(pengguna);

        cutiService.save(cuti);

        model.addAttribute("msg", "Pengajuan Cuti telah dibatalkan.");
        return "add-cuti";
    }

    @GetMapping("/kelola/terima/{noCuti}")
    public String terimaCuti(
            @PathVariable(value = "noCuti") long noCuti,
            Model model
    ) {
        CutiModel c = cutiService.getCutiByNoCuti(noCuti);

        if(c.getStatus().equals("Menunggu Persetujuan Supervisi")){
            c.setStatus("Menunggu Persetujuan Manager");
            model.addAttribute("Judul","Permohonan cuti diterima Supervisor");
            model.addAttribute("Isi","Permohonan cuti akan dialihkan ke Manager dan Direktur untuk persetujuan.");
            model.addAttribute("istolak",false);
            cutiService.save(c);
        } else if(c.getStatus().equals("Menunggu Persetujuan Manager")){
            c.setStatus("Menunggu Persetujuan Direktur");
            model.addAttribute("Judul","Permohonan cuti diterima Manager");
            model.addAttribute("Isi","Permohonan cuti akan dialihkan ke Direktur untuk persetujuan.");
            model.addAttribute("istolak",false);
            cutiService.save(c);
        } else if(c.getStatus().equals("Menunggu Persetujuan Direktur")){
            c.setStatus("Disetujui");
            model.addAttribute("Judul","Permohonan cuti diterima");
            model.addAttribute("Isi","Permohonan cuti dari "+ c.getPengguna().getNama_depan() + " "
                                + c.getPengguna().getNama_belakang() + " berhasil diterima.");
            model.addAttribute("istolak",false);
            cutiService.save(c);
        }

        return "success-kelola-cuti";
    }

    @PostMapping("/kelola/tolak/{noCuti}")
    public String tolakCuti(
            @RequestParam(name="alasan_penolakan") String alasan_penolakan,
            @PathVariable(value = "noCuti") long noCuti,
            Model model
    ) {
        CutiModel cuti = cutiService.getCutiByNoCuti(noCuti);
        PenggunaModel pengguna = penggunaService.getPenggunaByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        cuti.setAlasan_penolakan(alasan_penolakan);
        cuti.setStatus("Ditolak");
        cuti.setPenolak(pengguna.getNama_depan() + " " + pengguna.getNama_belakang() + ", " + pengguna.getJabatan());
        cutiService.save(cuti);

        model.addAttribute("Judul","Permohonan cuti berhasil ditolak");
        model.addAttribute("Isi","Permohonan cuti dari "+ cuti.getPengguna().getNama_depan() + " "
                + cuti.getPengguna().getNama_belakang() + " berhasil ditolak.");
        model.addAttribute("istolak",true);
        model.addAttribute("alasan_tolak", cuti.getAlasan_penolakan());
        return "success-kelola-cuti";
    }

}
