package com.sioffice.controller;

import com.sioffice.model.FileRegulasiEntity;
import com.sioffice.model.RegulasiModel;
import com.sioffice.service.RegulasiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/regulasi")
public class RegulasiController {
    @Qualifier("regulasiServiceImpl")
    @Autowired
    private RegulasiService regulasiService;

    @GetMapping("")
    public String daftarRegulasiPerusahaan(Model model,
                                           @RequestParam(value = "page", required = false) Integer page,
                                           @RequestParam(value = "size", required = false) Integer size) {
        if (page == null || size == null) {
            List<RegulasiModel> daftarRegulasi = regulasiService.getlistRegulasi();
            int a = (int) Math.ceil((double) daftarRegulasi.size()/5);
            daftarRegulasi = regulasiService.getListRegulasiByPageAndSize(0, 5);
            model.addAttribute("countPage", a);
            model.addAttribute("size", 5);
            model.addAttribute("page", 1);
            model.addAttribute("daftarRegulasi", daftarRegulasi);
        } else {
            List<RegulasiModel> daftarRegulasi = regulasiService.getlistRegulasi();
            int a = (int) Math.ceil((double) daftarRegulasi.size()/5);
            daftarRegulasi = regulasiService.getListRegulasiByPageAndSize(0, 5);
            model.addAttribute("countPage", a);
            model.addAttribute("size", size);
            model.addAttribute("page", page);
            model.addAttribute("daftarRegulasi", daftarRegulasi);
        }
        model.addAttribute("type", "search-none");
        return "viewall-regulasi";
    }

    @GetMapping("/unggah")
    public String formAddRegulasi(Model model) {
        return "form-add-regulasi";
    }

    @PostMapping("/unggah")
    public String submitAddRegulasi(Model model,
                                  @RequestParam(value = "nomor") String nomor,
                                  @RequestParam(value = "perihal") String perihal,
                                  @RequestParam(value = "fileRegulasi")MultipartFile file) throws IOException {
        RegulasiModel regulasi = new RegulasiModel();
        FileRegulasiEntity fileRegulasi = new FileRegulasiEntity();
        regulasi.setNomor(nomor);
        regulasi.setPerihal(perihal);
        regulasi.setTanggal(LocalDate.now());
        regulasi.setFileRegulasi(fileRegulasi);
        fileRegulasi.setRegulasi(regulasi);
        regulasiService.addRegulasi(regulasi);
        regulasiService.saveFileRegulasi(file, fileRegulasi);
        return "redirect:/regulasi";
    }

    @GetMapping("/search")
    public String searchRegulasi(Model model,
                                 @RequestParam(value = "criteria") Optional<String> criteria,
                                 @RequestParam(value = "input") Optional<Object> inputSearch,
                                 @RequestParam(value = "page", required = false) Integer page,
                                 @RequestParam(value = "size", required = false) Integer size) {
        String inputName;
        LocalDate inputDate;
        List<RegulasiModel> daftarRegulasi = new ArrayList<>();
        if (page == null || size == null) {
            if (criteria.isPresent() && criteria.get().equals("perihal")) {
                inputName = (String) inputSearch.get();
                List<RegulasiModel> p = regulasiService.getListRegulasiByPerihal(inputName);
                daftarRegulasi = regulasiService.getListRegulasiByPerihalAndPageAndSize(inputName, 0, 5);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
                model.addAttribute("type", "search-perihal");
                model.addAttribute("inputSearch", inputName);
            } else if (criteria.isPresent() && criteria.get().equals("tanggal")) {
                inputDate = LocalDate.parse((String) inputSearch.get());
                List<RegulasiModel> p = regulasiService.getListRegulasiByDate(inputDate);
                daftarRegulasi = regulasiService.getListRegulasiByDateAndPageAndSize(inputDate, 0, 5);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
                model.addAttribute("type", "search-tanggal");
                model.addAttribute("inputSearch", inputDate);
            }
        } else {
            if (criteria.isPresent() && criteria.get().equals("perihal")) {
                inputName = (String) inputSearch.get();
                List<RegulasiModel> p = regulasiService.getListRegulasiByPerihal(inputName);
                daftarRegulasi = regulasiService.getListRegulasiByPerihalAndPageAndSize(inputName, page-1, size);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
                model.addAttribute("type", "search-perihal");
                model.addAttribute("inputSearch", inputName);
            } else if (criteria.isPresent() && criteria.get().equals("tanggal")) {
                inputDate = LocalDate.parse((String) inputSearch.get());
                List<RegulasiModel> p = regulasiService.getListRegulasiByDate(inputDate);
                daftarRegulasi = regulasiService.getListRegulasiByDateAndPageAndSize(inputDate, page-1, size);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
                model.addAttribute("type", "search-tanggal");
                model.addAttribute("inputSearch", inputDate);
            }
        }
        model.addAttribute("daftarRegulasi", daftarRegulasi);
        return "viewall-regulasi";
    }

    @GetMapping("/edit/{id}")
    public String formEditRegulasi(Model model, @PathVariable(value = "id") Long id) {
        RegulasiModel regulasi = regulasiService.getRegulasi(id);
        FileRegulasiEntity fileRegulasi = regulasi.getFileRegulasi();
        MultipartFile file = regulasiService.convertFileRegulasiToMultiPart(fileRegulasi);
        model.addAttribute("regulasi", regulasi);
        model.addAttribute("file", file);
        return "form-edit-regulasi";
    }

    @PostMapping("/edit")
    public String submitEditRegulasi(Model model,
                                    @RequestParam(value = "idRegulasi") Long idRegulasi,
                                    @RequestParam(value = "nomor") String nomor,
                                    @RequestParam(value = "perihal") String perihal,
                                    @RequestParam(value = "selectFile") String selectFile,
                                    @RequestParam(value = "fileRegulasi", required = false)MultipartFile file) throws IOException {
        RegulasiModel regulasi = regulasiService.getRegulasi(idRegulasi);
        FileRegulasiEntity fileRegulasi = regulasi.getFileRegulasi();
        regulasi.setNomor(nomor);
        regulasi.setPerihal(perihal);
        regulasi.setTanggal(LocalDate.now());
        if (selectFile.equals("new-file")) {
            regulasi.setFileRegulasi(fileRegulasi);
            fileRegulasi.setRegulasi(regulasi);
            regulasiService.editRegulasi(regulasi);
            regulasiService.editFileRegulasi(file, fileRegulasi);
        } else {
            regulasiService.editRegulasi(regulasi);
        }
        return "redirect:/regulasi";
    }
}
