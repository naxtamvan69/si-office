package com.sioffice.controller;

import com.sioffice.model.PenggunaModel;
import com.sioffice.model.RoleModel;
import com.sioffice.model.DivisiModel;
import com.sioffice.service.KaryawanService;
import com.sioffice.service.RoleService;
import com.sioffice.service.DivisiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/datakaryawan")
public class DataKaryawanController {
    @Qualifier("karyawanServiceImpl")
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private DivisiService divisiService;

    @GetMapping("")
    public String datakaryawan(Model model) {
        List<PenggunaModel> daftaruser = karyawanService.listKaryawan();
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        String divisi = "";
        if(role.equals("[Direktur]")){
            divisi = "Direktur";
        } else if(role.equals("[Manager]")){
            divisi = "Manager";
        } else if(role.equals("[Supervisor]")){
            divisi = "Supervisor";
        } else if(role.equals("[Karyawan]")){
            divisi = "Karyawan";
        } else if(role.equals("[Manager]")){
            divisi = "Manager";
        } else if(role.equals("[SDM]")){
            divisi = "SDM";
        } else if(role.equals("[Operasional]")){
            divisi = "Operasional";
        } else if(role.equals("[Marketing]")){
            divisi = "Marketing";
        } else if(role.equals("[Katering RS]")){
            divisi = "Katering RS";
        }
        model.addAttribute("daftaruser", daftaruser);
        model.addAttribute("role", role);
        model.addAttribute("divisi", divisi);
        return "viewall-karyawan";
    }

    @GetMapping("/tambah")
    public String FormAddKaryawan(Model model) {
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("role", role);
        String divisi = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("divisi", divisi);

        PenggunaModel karyawan = new PenggunaModel();
        List<RoleModel> penggunaRole = roleService.getListAllRole();
        List<DivisiModel> penggunaDivisi = divisiService.listDivisi();

        model.addAttribute("karyawan", karyawan);
        model.addAttribute("penggunaRole", penggunaRole);
        model.addAttribute("penggunaDivisi", penggunaDivisi);
        return "form-add-karyawan";
    }

    @PostMapping("/tambah")
    public String submitAddKaryawan(Model model,
                                    @ModelAttribute PenggunaModel karyawan) {
        karyawan.setTtdPengguna(null);
        karyawan.setFotoPengguna(null);
        karyawan.setSisa_cuti(12);
        karyawanService.addKaryawan(karyawan);
        model.addAttribute("karyawan", karyawan);
        model.addAttribute("username", karyawan.getUsername());
        return "addkaryawan";
    }

    @GetMapping("/update/{id}")
    public String FormUpdateKaryawan(
            @PathVariable Integer id,
            Model model) {
        PenggunaModel karyawan = karyawanService.getKaryawanById(id);
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("role", role);
        String divisi = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("divisi", divisi);

        List<RoleModel> penggunaRole = roleService.getListAllRole();
        List<DivisiModel> penggunaDivisi = divisiService.listDivisi();

        model.addAttribute("karyawan", karyawan);
        model.addAttribute("penggunaRole", penggunaRole);
        model.addAttribute("penggunaDivisi", penggunaDivisi);
        return "form-update-karyawan";
    }

    @PostMapping("/update")
    public String UpdateKaryawan(
            @ModelAttribute PenggunaModel karyawan,
            Model model
    ){
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("role", role);
        model.addAttribute("karyawan", karyawan);
        karyawan.setSisa_cuti(12);
        karyawanService.updateKaryawan(karyawan);
        model.addAttribute("id", karyawan.getId());
        model.addAttribute("username", karyawan.getUsername());
        return "update-karyawan";
    }

    @GetMapping("/delete/{id}")
    public String DeleteKaryawan(
            @PathVariable (value = "id", required = true) Integer id,
            @ModelAttribute("message") Object message,
            RedirectAttributes rd,
            Model model
    ){
        PenggunaModel karyawan = karyawanService.getKaryawanById(id);
        message = "Karyawan " + karyawan.getNama_depan() + " " + karyawan.getNama_belakang() + " dengan ID " + karyawan.getId() + " telah berhasil dihapus";
        karyawanService.deleteKaryawan(karyawan);
        rd.addFlashAttribute("alertMessage", true);
        rd.addFlashAttribute("message", message);
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        model.addAttribute("role", role);
        return "redirect:/datakaryawan";
    }

    @GetMapping("/view/{id}")
    public String viewKaryawan(
            @PathVariable Integer id,
            Model model
    ){
        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
        String divisi = "";
        if(role.equals("[Direktur]")){
            divisi = "Direktur";
        } else if(role.equals("[Manager]")){
            divisi = "Manager";
        } else if(role.equals("[Supervisor]")){
            divisi = "Supervisor";
        } else if(role.equals("[Karyawan]")){
            divisi = "Karyawan";
        } else if(role.equals("[Manager]")){
            divisi = "Manager";
        } else if(role.equals("[SDM]")){
            divisi = "SDM";
        } else if(role.equals("[Operasional]")){
            divisi = "Operasional";
        } else if(role.equals("[Marketing]")){
            divisi = "Marketing";
        } else if(role.equals("[Katering RS]")){
            divisi = "Katering RS";
        }
        model.addAttribute("karyawan", karyawanService.getKaryawanById(id));
        model.addAttribute("role", role);
        model.addAttribute("divisi", divisi);
        return "view-karyawan";
    }
}
