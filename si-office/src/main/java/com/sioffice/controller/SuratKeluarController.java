package com.sioffice.controller;

import com.sioffice.model.FileSuratKeluarEntity;
import com.sioffice.model.SuratKeluarModel;
import com.sioffice.service.SuratKeluarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping("/arsip/surat-keluar")
public class SuratKeluarController {
    @Qualifier("suratKeluarServiceImpl")
    @Autowired
    private SuratKeluarService suratKeluarService;

    @GetMapping("/unggah")
    public String formUnggahSuratKeluar(Model model) {
        return "form-unggah-surat-keluar";
    }

    @PostMapping("/unggah")
    public String StringSubmitUnggahSuratKeluar(Model model,
                                  @RequestParam(value = "nomor") String nomor,
                                  @RequestParam(value = "perihal") String perihal,
                                  @RequestParam(value = "pengirim") String pengirim,
                                  @RequestParam(value = "penerima") String penerima,
                                  @RequestParam(value = "fileSuratKeluar") MultipartFile file) throws IOException {
        SuratKeluarModel suratKeluar = new SuratKeluarModel();
        FileSuratKeluarEntity fileSuratKeluar = new FileSuratKeluarEntity();
        suratKeluar.setNomor(nomor);
        suratKeluar.setPerihal(perihal);
        suratKeluar.setTanggal(LocalDate.now());
        suratKeluar.setPengirim(pengirim);
        suratKeluar.setPenerima(penerima);
        suratKeluar.setFileSuratKeluar(fileSuratKeluar);
        fileSuratKeluar.setSuratKeluar(suratKeluar);
        suratKeluarService.saveSuratKeluar(suratKeluar);
        suratKeluarService.saveFileSuratKeluar(file, fileSuratKeluar);
        return "redirect:/arsip/surat-keluar";
    }

    @GetMapping("/edit/{id}")
    public String formEditSuratKeluar(Model model,
                                      @PathVariable(value = "id") Long id) {
        SuratKeluarModel suratKeluar = suratKeluarService.getSuratKeluar(id);
        model.addAttribute("suratKeluar", suratKeluar);
        return "form-edit-surat-keluar";
    }

    @PostMapping("/edit")
    public String submitEditSuratKeluar(@RequestParam(value = "idSuratKeluar") Long idSuratKeluar,
                                        @RequestParam(value = "nomor") String nomor,
                                        @RequestParam(value = "perihal") String perihal,
                                        @RequestParam(value = "pengirim") String pengirim,
                                        @RequestParam(value = "penerima") String penerima,
                                        @RequestParam(value = "fileSuratKeluar", required = false) MultipartFile file,
                                        @RequestParam(value = "selectFile") String selectFile) throws IOException {
        SuratKeluarModel suratKeluar = suratKeluarService.getSuratKeluar(idSuratKeluar);
        FileSuratKeluarEntity fileSuratKeluar = suratKeluar.getFileSuratKeluar();
        suratKeluar.setNomor(nomor);
        suratKeluar.setPerihal(perihal);
        suratKeluar.setTanggal(LocalDate.now());
        suratKeluar.setPengirim(pengirim);
        suratKeluar.setPenerima(penerima);
        if (selectFile.equals("new-file")) {
            suratKeluar.setFileSuratKeluar(fileSuratKeluar);
            fileSuratKeluar.setSuratKeluar(suratKeluar);
            suratKeluarService.editSuratKeluar(suratKeluar);
            suratKeluarService.editFileSuratKeluar(file, fileSuratKeluar);
        } else {
            suratKeluarService.editSuratKeluar(suratKeluar);
        }
        return "redirect:/arsip/surat-keluar";
    }

    @GetMapping("")
    public String dataSuratKeluar(Model model,
                                  @RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "size", required = false) Integer size){
        if (page == null || size == null) {
            List<SuratKeluarModel> dataSuratKeluar = suratKeluarService.getListSuratKeluarByPageAndSize(0, 5);
            List<SuratKeluarModel> p = suratKeluarService.getListSuratKeluar();
            int count = (int) Math.ceil((double) p.size()/5);
            model.addAttribute("daftarSuratKeluar", dataSuratKeluar);
            model.addAttribute("countPage", count);
            model.addAttribute("size", 5);
            model.addAttribute("page", 1);
        } else {
            List<SuratKeluarModel> dataSuratKeluar = suratKeluarService.getListSuratKeluarByPageAndSize(page-1, 5);
            List<SuratKeluarModel> p = suratKeluarService.getListSuratKeluar();
            int count = (int) Math.ceil((double) p.size()/size);
            model.addAttribute("daftarSuratKeluar", dataSuratKeluar);
            model.addAttribute("countPage", count);
            model.addAttribute("size", size);
            model.addAttribute("page", page);
        }
        model.addAttribute("type", "search-none");
        return "viewall-surat-keluar";
    }

    @GetMapping("/search")
    public String searchSuratKeluar(Model model,
                                   @RequestParam(value = "criteria") Optional<String> criteria,
                                   @RequestParam(value = "input") Optional<Object> inputSearch,
                                   @RequestParam(value = "page", required = false) Integer page,
                                   @RequestParam(value = "size", required = false) Integer size) {
        String inputName;
        LocalDate inputDate;
        List<SuratKeluarModel> daftarSuratKeluar = new ArrayList<>();
        if (page == null || size == null) {
            if (criteria.isPresent() && criteria.get().equals("perihal")) {
                inputName = (String) inputSearch.get();
                daftarSuratKeluar = suratKeluarService.getListSuratKeluarByPageAndSize(inputName, 0, 5);
                List<SuratKeluarModel> p = suratKeluarService.getListSuratKeluar(inputName);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
                model.addAttribute("type", "search-perihal");
                model.addAttribute("inputSearch", inputName);
            } else if (criteria.isPresent() && criteria.get().equals("tanggal")) {
                inputDate = LocalDate.parse((String) inputSearch.get());
                daftarSuratKeluar = suratKeluarService.getListSuratKeluarByPageAndSize(inputDate, 0, 5);
                List<SuratKeluarModel> p = suratKeluarService.getListSuratKeluar(inputDate);
                int a = (int) Math.ceil((double) p.size()/5);
                model.addAttribute("countPage", a);
                model.addAttribute("size", 5);
                model.addAttribute("page", 1);
                model.addAttribute("type", "search-tanggal");
                model.addAttribute("inputSearch", inputDate);
            }
        } else {
            if (criteria.isPresent() && criteria.get().equals("perihal")) {
                inputName = (String) inputSearch.get();
                daftarSuratKeluar = suratKeluarService.getListSuratKeluarByPageAndSize(inputName, page-1, size);
                List<SuratKeluarModel> p = suratKeluarService.getListSuratKeluar(inputName);
                int a = (int) Math.ceil((double) p.size()/size);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
                model.addAttribute("type", "search-perihal");
                model.addAttribute("inputSearch", inputName);
            } else if (criteria.isPresent() && criteria.get().equals("tanggal")) {
                inputDate = LocalDate.parse((String) inputSearch.get());
                daftarSuratKeluar = suratKeluarService.getListSuratKeluarByPageAndSize(inputDate, page-1, size);
                List<SuratKeluarModel> p = suratKeluarService.getListSuratKeluar(inputDate);
                int a = (int) Math.ceil((double) p.size()/size);
                model.addAttribute("countPage", a);
                model.addAttribute("size", size);
                model.addAttribute("page", page);
                model.addAttribute("type", "search-tanggal");
                model.addAttribute("inputSearch", inputDate);
            }
        }
        model.addAttribute("daftarSuratKeluar", daftarSuratKeluar);
        return "viewall-surat-keluar";
    }

}
